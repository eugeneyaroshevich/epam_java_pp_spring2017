package by.training.customerapi.controller;

import by.training.customerapi.entity.Customer;
import by.training.exception.ClusterException;
import by.training.customerapi.exception.CustomerException;
import by.training.parser.JsonParser;
import by.training.customerapi.service.CustomerService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class CustomerServlet extends HttpServlet {

    private CustomerService customerService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json");

        HttpSession session = req.getSession();

        try {
            String action = req.getParameter("action");

            switch (action) {
                case "getCustomer":
                    changeCache(req, session);
                    getCustomer(req, resp);
                    break;

                case "showCustomer":
                    req.getRequestDispatcher("/showCustomer.jsp").forward(req, resp);
                    break;

                default:
                    req.getRequestDispatcher("/showCustomer.jsp").forward(req, resp);
                    break;
            }
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json");

        HttpSession session = req.getSession();

        try {
            String action = req.getParameter("action");

            switch (action) {

                case "createCustomer":
                    changeCache(req, session);
                    createCustomer(req, resp);
                    break;
                default:
                    req.getRequestDispatcher("/showCustomer.jsp").forward(req, resp);
                    break;
            }
        } catch (Exception e) {
            throw new ServletException(e);
        }

    }

    private void createCustomer(HttpServletRequest req, HttpServletResponse resp) throws CustomerException, IOException, ClusterException {

        String jsonCustomer = (String) req.getAttribute("jsonObject");
        Customer customer = JsonParser.fromJson(jsonCustomer, Customer.class);
        customerService.add(customer);
        resp.sendRedirect("/showCustomer.jsp");
    }

    private void getCustomer(HttpServletRequest req, HttpServletResponse resp) throws CustomerException, ServletException, IOException, ClusterException {

        String jsonCustomerId = (String) req.getAttribute("jsonObject");
        int id = JsonParser.toIdFromJsonCustomer(jsonCustomerId);
        Customer customer = customerService.getById(id);

        req.setAttribute("customer", customer);
        RequestDispatcher dispatcher = req.getRequestDispatcher("/getCustomer.jsp");
        dispatcher.forward(req, resp);

//        or response json to client (js)

//        String responseJson = JsonParser.toJson(customer);
//        resp.getWriter().write(responseJson);
    }

    private void changeCache(HttpServletRequest req, HttpSession session) throws CustomerException {

        String cache = req.getParameter("cache");

        if ("lfu".equals(cache)) {
            customerService = (CustomerService) session.getAttribute("lfuCustomerService");
        } else if ("lru".equals(cache)) {
            customerService = (CustomerService) session.getAttribute("lruCustomerService");
        } else {
            throw new CustomerException("Inter cache's name: lfu or lru");
        }
    }

}
