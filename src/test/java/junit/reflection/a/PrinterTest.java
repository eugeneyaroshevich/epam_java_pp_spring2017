package junit.reflection.a;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import by.training.reflection.a.Printable;
import by.training.reflection.a.Printer;

import static org.junit.Assert.*;

public class PrinterTest {

    private Printable printer;

    @Before
    public void before() {
        printer = new Printer();
    }

    @After
    public void after() {
        printer = null;
    }

    @Test
    public void testNewInstansePrinterNotNullReturned(){
        Printable printer = new Printer();

        assertNotNull(printer);
    }


    @Test
    public void testPrintInArgNotNullReturned(){

        String message = "Test";

        assertNotNull(printer.print(message));
    }

    @Test
    public void testPrintInArgEqualReturned(){

        String message = "Test";

        assertEquals(message,printer.print(message));
    }
}