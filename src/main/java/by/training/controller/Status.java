package by.training.controller;

import by.training.context.ClusterContext;
import by.training.exception.ClusterException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static by.training.constants.Constants.*;
import static by.training.util.ClusterUtil.getMethod;


public class Status extends HttpServlet {

    private static final Logger log = Logger.getLogger(Status.class);

    @Autowired
    protected ClusterContext context;

    @Override
    public void init() throws ServletException {
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            String parameter = request.getParameter(STATUS);
            if (parameter == null) {
                throw new ClusterException(NOT_STATUS_PARAMETER);
            }

            String status;
            if (CLUSTER.equals(parameter)) {
                status = String.valueOf(context.isNodeStatuses());
            } else if (SYNCHRONIZE.equals(parameter)) {
                status = String.valueOf(context.isSynchroStatus());
            }else {
                status = String.valueOf(context.getNodeStatuses().get(parameter));
                if (status == null) {
                    throw new ClusterException(NOT_STATUS_CHECK_ADDRESS);
                }
            }
            response.getWriter().write(status);

        } catch (ClusterException e) {
            log.warn(this.getClass().getName() + getMethod(request)+ FAILED, e);
            throw new ServletException(e);
        }
    }
}