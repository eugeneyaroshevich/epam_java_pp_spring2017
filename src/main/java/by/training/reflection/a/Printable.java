package by.training.reflection.a;

public interface Printable {

    String print(String string);
}
