package by.training.customerapi.dao;

import by.training.customerapi.entity.Customer;
import by.training.cache.Cache;
import by.training.reflection.b.SerializationUtils;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Map;

public class CustomerDaoImplProxyHandler implements InvocationHandler {

    private Cache<String, Map<String, Object>> cache;
    private CustomerDao customerDao;
    
    public CustomerDaoImplProxyHandler(CustomerDao customerDao, Cache<String, Map<String, Object>> cache) {
        this.customerDao = customerDao;
        this.cache = cache;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        Object result;
        Customer customer;

        if ("getById".equals(method.getName())) {
            if (cache.get("customer" + args[0]) != null) {
                customer = SerializationUtils.deserialize(cache.get("customer" + args[0]), Customer.class);
                result = customer;
            } else {
                customer = customerDao.getById((Integer) args[0]);
                cache.put("customer" + args[0], SerializationUtils.serialize(customer));
                result = customer;
            }
            return result;
        }
        if ("addOrUpdate".equals(method.getName())) {

            customer = (Customer) args[0];
            Map<String, Object> oldCustomer;
            if (cache.get("customer" + customer.getId()) != null) {
                oldCustomer = cache.put("customer" + customer.getId(), SerializationUtils.serialize(customer));
//                customerDao.update(customer);
                result = SerializationUtils.deserialize(oldCustomer, Customer.class);
            } else {
                oldCustomer = cache.put("customer" + customer.getId(), SerializationUtils.serialize(customer));
                customerDao.add(customer);
                result = oldCustomer;
            }
            return result;
        }

        result = method.invoke(customerDao, args);


        return result;
    }
}
