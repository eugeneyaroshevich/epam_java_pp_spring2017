//package by.training.customerapi.test.service;
//
//import by.training.customerapi.entity.Customer;
//import by.training.cache.Cache;
//import by.training.cache.ConcurrentLFUCache;
//import by.training.cache.ConcurrentLRUCache;
//import by.training.customerapi.dao.CustomerDao;
//import by.training.customerapi.dao.CustomerDaoImpl;
//import org.junit.Before;
//import org.junit.Test;
//import by.training.reflection.a.ProxyFactory;
//import by.training.reflection.b.SerializationUtils;
//import by.training.customerapi.service.CustomerService;
//import by.training.customerapi.service.CustomerServiceImpl;
//import by.training.customerapi.service.ServiceFactory;
//
//import static org.junit.Assert.*;
//
//public class CustomerServiceImplTest {
//
//    private  CustomerServiceImpl lfuCustomerService;
//    private  CustomerServiceImpl lruCustomerService;
//    private  Customer customer1;
//    private  Customer customer1Changed;
//    private  Customer customer2;
//
//    @Before
//    public  void setUp() throws Exception {
//
//        customer1 = new Customer(0, "Bean1", "Binich1", 40);
//        customer1Changed = new Customer(0, "Bean1Changed", "Binich1Changed", 30);
//        customer2 = new Customer(1, "Bean2", "Binich2", 41);
//
//        Cache lfuCache = new ConcurrentLFUCache<>();
//        Cache lruCache = new ConcurrentLRUCache<>(3, 0.75f);
//
//        lfuCache.put("customer0", SerializationUtils.serialize(customer1));
//
//        lruCache.put("customer0", SerializationUtils.serialize(customer1));
//
//        CustomerDao lfuCustomerDaoImpl = (CustomerDao) ProxyFactory.getInstanceOf(CustomerDaoImpl.class, lfuCache);
//
//        lfuCustomerService = new CustomerServiceImpl();
//        lfuCustomerService.setCustomerDao(lfuCustomerDaoImpl);
//
//        CustomerDao lruCustomerDaoImpl = (CustomerDao) ProxyFactory.getInstanceOf(CustomerDaoImpl.class, lruCache);
//        lruCustomerService = new CustomerServiceImpl();
//        lruCustomerService.setCustomerDao(lruCustomerDaoImpl);
//
//    }
//
//    // ServiceFactory class new Instance
//    @Test
//    public void testNewInstanceServiceFactory(){
//
//        ServiceFactory serviceFactory = new ServiceFactory();
//
//        assertNotNull(serviceFactory);
//    }
//
//    // TestCustomerException class new Instance and ServiceFactory class ServiceFactory.getCustomerDaoImpl method
//    @Test
//    public void testNewInstanceFromServiceFactorygetCustomerDAO(){
//        CustomerService customerService = ServiceFactory.getCustomerServiceImpl();
//
//        assertNotNull(customerService);
//        assertTrue(customerService instanceof CustomerServiceImpl);
//    }
//
//    @Test
//    public void testAddLFUReturnedValuesEqualOriginal() throws Exception {
//
//        lfuCustomerService.add(customer2);
//
//        Customer customer = lfuCustomerService.getById(1);
//
//        assertNotNull(customer);
//        assertEquals(1, customer.getId());
//        assertEquals("Bean2", customer.getName());
//        assertEquals("Binich2",customer.getSurname());
//        assertEquals(41,customer.getAge());
//        assertEquals(customer,customer2);
//    }
//
//    @Test
//    public void testAddAndChangeLFUReturnedValuesEqualOriginal() throws Exception {
//
//        Customer oldCustomer = lfuCustomerService.getById(0);
//
//        assertNotNull(oldCustomer);
//        assertEquals(0, oldCustomer.getId());
//        assertEquals("Bean1", oldCustomer.getName());
//        assertEquals("Binich1", oldCustomer.getSurname());
//        assertEquals(40, oldCustomer.getAge());
//        assertEquals(oldCustomer, customer1);
//
//
//        lfuCustomerService.add(customer1Changed);
//
//        Customer customerUpdated = lfuCustomerService.getById(0);
//
//        assertNotNull(customerUpdated);
//        assertNotEquals(oldCustomer,customerUpdated);
//        assertEquals(oldCustomer.getId(),customerUpdated.getId());
//        assertEquals(0,customerUpdated.getId());
//        assertEquals("Bean1Changed",customerUpdated.getName());
//        assertEquals("Binich1Changed",customerUpdated.getSurname());
//        assertEquals(30,customerUpdated.getAge());
//
//    }
//
//    @Test
//    public void testAddLRUReturnedValuesEqualOriginal() throws Exception {
//
//        lruCustomerService.add(customer2);
//
//        Customer customer = lruCustomerService.getById(1);
//
//        assertNotNull(customer);
//        assertEquals(1, customer.getId());
//        assertEquals("Bean2", customer.getName());
//        assertEquals("Binich2",customer.getSurname());
//        assertEquals(41,customer.getAge());
//        assertEquals(customer,customer2);
//    }
//
//    @Test
//    public void testAddAndChangeLRUReturnedValuesEqualOriginal() throws Exception {
//
//        Customer oldCustomer = lruCustomerService.getById(0);
//
//        assertNotNull(oldCustomer);
//        assertEquals(0, oldCustomer.getId());
//        assertEquals("Bean1", oldCustomer.getName());
//        assertEquals("Binich1", oldCustomer.getSurname());
//        assertEquals(40, oldCustomer.getAge());
//        assertEquals(oldCustomer, customer1);
//
//
//        lruCustomerService.add(customer1Changed);
//
//        Customer customerUpdated = lruCustomerService.getById(0);
//
//        assertNotNull(customerUpdated);
//        assertNotEquals(oldCustomer,customerUpdated);
//        assertEquals(oldCustomer.getId(),customerUpdated.getId());
//        assertEquals(0,customerUpdated.getId());
//        assertEquals("Bean1Changed",customerUpdated.getName());
//        assertEquals("Binich1Changed",customerUpdated.getSurname());
//        assertEquals(30,customerUpdated.getAge());
//
//    }
//
//    @Test
//    public void testGetByIdLFUReturnedValuesEqualOriginal() throws Exception {
//        Customer customer = lfuCustomerService.getById(0);
//
//        assertNotNull(customer);
//        assertEquals(0, customer.getId());
//        assertEquals("Bean1", customer.getName());
//        assertEquals("Binich1", customer.getSurname());
//        assertEquals(40, customer.getAge());
//        assertEquals(customer, customer1);
//    }
//    @Test
//    public void testGetByIdLRUReturnedValuesEqualOriginal() throws Exception {
//        Customer customer = lruCustomerService.getById(0);
//
//        assertNotNull(customer);
//        assertEquals(0, customer.getId());
//        assertEquals("Bean1", customer.getName());
//        assertEquals("Binich1", customer.getSurname());
//        assertEquals(40, customer.getAge());
//        assertEquals(customer, customer1);
//    }
//
//}