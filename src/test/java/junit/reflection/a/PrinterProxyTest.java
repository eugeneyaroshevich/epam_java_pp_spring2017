package junit.reflection.a;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import by.training.reflection.a.Printable;
import by.training.reflection.a.Printer;
import by.training.reflection.a.PrinterProxy;

import static org.junit.Assert.*;

public class PrinterProxyTest {

    private PrinterProxy printer;

    @Before
    public void before() {
        printer = new PrinterProxy();
    }

    @After
    public void after() {
        printer = null;
    }


    // new Instance
    @Test
    public void testNewInstancePrinterNotNullReturned(){
        Printable printer = new PrinterProxy();

        assertNotNull(printer);
    }

    @Test
    public void testNewInstanceAfterInstanciateFieldRealPrinterNullReturned(){

        assertNull(printer.getRealPrinter());
    }

    // get / set realPrinter()
    @Test
    public void testSetRealPrinterTrueInAndNotNullReturned(){

        printer.setRealPrinter(new Printer());
        assertNotNull(printer.getRealPrinter());
    }

    @Test
    public void testSetRealPrinterNullInAndNullReturned(){

        printer.setRealPrinter(null);
        assertNull(printer.getRealPrinter());
    }


    // print() method
    @Test
    public void testPrintInArgNotNullReturned(){

        String message = "Test";

        assertNotNull(printer.print(message));
    }

    @Test
    public void testPrintInArgEqualReturned(){

        String message = "Test";

        assertEquals("TestProxy",printer.print(message));
    }


}