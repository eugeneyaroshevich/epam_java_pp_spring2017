package by.training.reflection.a;

import by.training.cache.Cache;

import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.util.Map;

public class ProxyFactory {

    public static Object getInstanceOf(Class<?> clazz) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        return getProxy(clazz, null);
    }

    public static <T extends Comparable<T>> Object getInstanceOf(Class<?> clazz, Cache<T, Map<String, Object>> cache) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        return getProxy(clazz, cache);
    }

    private static boolean isProxyClass(Class<?> clazz) {
        return clazz.isAnnotationPresent(Proxy.class);
    }

    private static Object newProxyInstance(Class<?> clazz, InvocationHandler handler) {
        return java.lang.reflect.Proxy.newProxyInstance(
                clazz.getClassLoader(),
                clazz.getInterfaces(),
                handler);
    }

    private static Class<?> getHandlerClass(Class<?> clazz) throws ClassNotFoundException {
        Annotation annotation = clazz.getAnnotation(Proxy.class);
        Proxy proxy = (Proxy) annotation;
        return Class.forName(proxy.invocationHandler());
    }

    private static <T extends Comparable<T>> Object getProxy(Class<?> clazz, Cache<T, Map<String, Object>> cache) throws IllegalAccessException, InstantiationException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException {
        if (!isProxyClass(clazz)) {
            return clazz.newInstance();
        }
        Class<?> handlerClass = getHandlerClass(clazz);
        Constructor<?> constructor;
        InvocationHandler handler;
        Class<?> parameterType1 = getHandlerClass(clazz).getConstructors()[0].getParameterTypes()[0];
        if (cache == null) {
            constructor = handlerClass.getConstructor(parameterType1);
            handler = (InvocationHandler) constructor.newInstance(clazz.newInstance());
        } else {
            Class<?> parameterType2 = getHandlerClass(clazz).getConstructors()[0].getParameterTypes()[1];
            constructor = handlerClass.getConstructor(parameterType1, parameterType2);
            handler = (InvocationHandler) constructor.newInstance(clazz.newInstance(), cache);
        }

        return newProxyInstance(clazz, handler);
    }
}


