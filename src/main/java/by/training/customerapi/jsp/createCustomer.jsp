<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Create New Customer</title>
</head>
<body>
<form method="POST" action='customer' name="frmCreateCustomer"><input
        type="hidden" name="action" value="createCustomer" />
    <p><b>Add New Record</b></p>
    <table>
        <tr>
            <td>Type of cache</td>
            <td><input type="text" name="cache" /></td>
        </tr>
        <tr>
            <td>Customer ID</td>
            <td><input type="text" name="id" /></td>
        </tr>
        <tr>
            <td>Name</td>
            <td><input type="text" name="name" /></td>
        </tr>
        <tr>
            <td>Surname</td>
            <td><input type="text" name="surname" /></td>
        </tr>
        <tr>
            <td>Age</td>
            <td><input type="text" name="age" /></td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" value="Submit" /></td>
        </tr>
    </table>
</form>
<p><a href="/customer?action=showCustomer">Get Customer by ID</a></p>
</body>
</html>