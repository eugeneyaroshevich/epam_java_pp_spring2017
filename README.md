#web application cluster

**replication**

**wait ready work nodes**

**status REST api**

**synchronize**



 * author: Eugeny Yaroshevich
 * version: 1.0
 * date: 2017/08/31
 * **this program is free software; you can redistribute it and/or modify**

###example of deploying a cluster on a Apache Tomcat web server:

__1__. [download](https://bitbucket.org/evgeniy_yaroshevich/epam_java_pp_spring2017/get/3389ef4ca430.zip) repozitory

__2__. go to the folder where the repository with the application was downloaded and unpack the archive

__3__. at a command prompt, go to the folder with the unpacked archive (*for windows*: win+r -> cmd -> cd /d path_to_your_api)

__4__. go to the resources folder and enter the required data in the properties files or leave the default settings
(*by default, an application cluster consisting of two main nodes with one replication per node will be deployed*):
*****
>**app.properties:**
>>
>>*#datasource*
>>
>>* jdbc.driver=org.h2.Driver                         - default
>>* jdbc.url=jdbc:h2:~/db
>>
>>*at run time, a separate database will be created for each node and each replication*
>>*the name of the database will be the name of the war file (context path)*
>>
>>*__example:__* name war-file = node0.war ==> name database = node0.mv (for H2 db)
>>
>>* jdbc.username=ss                                  - default for H2
>>* jdbc.password=                                    - default
>>
>>*#hibernate*
>>
>>* hibernate.dialect=org.hibernate.dialect.H2Dialect - default
>>* hibernate.hbm2ddl.auto=update                     - default
>>* hibernate.connection.pool_size=50                 - default
>>* hibernate.current_session_context_class=thread    - default
>>* hibernate.show_sql=true                           - default
>
*****
>**cache.properties:**
>>
>>* cache=lfu                                         - default  // *choice of caching type*
>>* size=5                                            - default  // *size of cache*
>>* loadfactor=0.75f                                  - default  // *loadfactor of cache*
>
*****
>**log4j.properties:**
>>
>>* log4j.rootLogger=TRACE, file                      - default
>>* log4j.logger.org.hibernate=off                    - default
>>* log4j.logger.org.springframework=off              - default
>>* log4j.appender.file=org.apache.log4j.RollingFileAppender- default
>>* log4j.appender.file.File= ~//log_file.log         - default  // *set your path where the log file will be created*
>>* log4j.appender.file.MaxFileSize=1MB- default
>>* log4j.appender.file.layout=org.apache.log4j.PatternLayout                                             - default
>>* log4j.appender.file.layout.ConversionPattern=%d{yyyy-MM-dd HH:mm:ss} [%-5p][%-16.16t][%32.32c] - %m%n - default
>
*****
>**status.properties:**
>>
>>* clusterStatus=false                               - default
>>
>>*only when all the cluster members are active and the cluster is ready for operation - set to true (rewrite)*
>
*****
>**node.properties:**
>>
>>* http://127.0.0.1:8081/node0/node = 0                - default
>>* http://127.0.0.1:8082/node1/node = 0                - default
>>* http://127.0.0.1:8083/node2/node = 1                - default
>>* http://127.0.0.1:8084/node3/node = 1                - default
>>
>>*where "http://127.0.0.1:8081/node0/node" - default url for first node of cluster*
>>
>>*and "0" the index generated based on the key for the stored value to which the cluster node will match*
>
*****
>**timer.properties:**
>>
>>* initialDelay = 0 - default  // *through this time the cluster will collect statistics*
>>* period=3 - default  // *period with which the cluster will repeat the collection of statistics*
>
*****
__5__. being in the folder with the unpacked repository with the application, using Maven, compile and package .war-archive of your application (mvn package)

__6__. download and unpack the archive with [Apache Tomcat](http://tomcat.apache.org) corresponding to your operating system

__7__. in the unpacked distribution Apache Tomcat folder, edit the file \conf\server.xml,
   there you need to create additional services for the elements of your cluster

*****
__*example server.xml for the default option:*__

   <?xml version='1.0' encoding='utf-8'?>
  .......default body........

 <Server port="8005" shutdown="SHUTDOWN">
  .......default body........

 <Service>
  .......default body........
</Service>

__*this add next*__

<Service name="node0">
    <Connector port="8081" protocol="HTTP/1.1" />
    <Engine name="Catalina81" defaultHost="localhost">
        <Host name="localhost" appBase="node0" unpackWARs="true" autoDeploy="true" />
    </Engine>
</Service>

<Service name="node1">
    <Connector port="8082" protocol="HTTP/1.1" />
    <Engine name="Catalina82" defaultHost="localhost">
        <Host name="localhost" appBase="node1" unpackWARs="true" autoDeploy="true" />
    </Engine>
</Service>

<Service name="replica0">
    <Connector port="8083" protocol="HTTP/1.1" />
    <Engine name="Catalina83" defaultHost="localhost">
        <Host name="localhost" appBase="node2" unpackWARs="true" autoDeploy="true" />
    </Engine>
</Service>

<Service name="replica1">
    <Connector port="8084" protocol="HTTP/1.1" />
    <Engine name="Catalina84" defaultHost="localhost">
        <Host name="localhost" appBase="node3" unpackWARs="true" autoDeploy="true" />
    </Engine>
</Service>

</Server>

__*save this update*__
*****

__8__. for each element of the cluster in the root tomcat it is necessary to create the base directories
corresponding to appBase="..." of services added to the file server.xml:

*apache_tomcat_directory/folder with name equals value of appBase="..." in server.xml*

__9__. copy the packaged .war-file into each created directory
and rename the archive according to the values specified in the files node/replica.properties located in the resource folder:

*default value must be: node0.war, node1.war, node2.war, node3.war*

__10__. set environment variables

*at a command prompt input for each node, for default:*

>>         SETX KEY (context path) VALUE (url from node.properties)
>>* SETX node0                   http://127.0.0.1:8081/node0/node
>>* SETX node1                   http://127.0.0.1:8082/node1/node
>>* SETX node2                   http://127.0.0.1:8083/node2/node
>>* SETX node3                   http://127.0.0.1:8084/node3/node

__11__. run apache_tomcat_directory/bin/startUp.bat file (for windows)

__12__. for using cluster use any of the suggested node addresses