package by.training.util;

import by.training.entity.ACLEntity;
import by.training.exception.ClusterException;
import org.apache.http.client.methods.HttpDelete;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.util.*;

import static by.training.constants.Constants.*;

public final class ClusterUtil {

    private static final Logger log = Logger.getLogger(ClusterUtil.class);


    public static void setEncodingType(HttpServletRequest request, HttpServletResponse response) throws ClusterException {
        try {
            request.setCharacterEncoding(UTF_8);
            response.setCharacterEncoding(UTF_8);
            response.setContentType(APPLICATION_JSON);
        } catch (UnsupportedEncodingException e) {
            throw new ClusterException(SET_ENCODING_TYPE + FAILED, e);
        }
    }

    public static Map<String, String> getRequestParams(HttpServletRequest request) {
        Map<String, String> parameters = new HashMap<>();
        Enumeration parameterNames = request.getParameterNames();
        while (parameterNames.hasMoreElements()) {
            String key = (String) parameterNames.nextElement();
            String value = request.getParameter(key);
            parameters.put(key, value);
        }
        return parameters;
    }

    public static Map<String, String> createRequestParams(String key, String data, String synchro) {
        Map<String, String> parameters = new HashMap<>();
        parameters.put(ID,key);
        parameters.put(JSON,data);
        parameters.put(SYNCHRO,synchro);
        return parameters;
    }

    public static String getKey(HttpServletRequest request) {
        String requestKey = request.getParameter(ID);
        if (requestKey == null) {
            log.error(HAVE_T_KEY_IN_REQUEST);
            throw new NullPointerException(HAVE_T_KEY_IN_REQUEST);
        }
        return request.getParameter(ID);
    }

    public static String getData(HttpServletRequest req) {
        return req.getParameter(JSON);
    }

    public static String getMethod(HttpServletRequest req) {
        String method = req.getMethod();
        String hiddenMethod = req.getParameter(METHOD);
        if (hiddenMethod != null && HttpDelete.METHOD_NAME.equals(hiddenMethod)) {
            method = hiddenMethod;
        }
        return method;
    }

    public static int hash(Object key) {
        return (key == null) ? ZERO : key.hashCode();
    }

    public static int indexFor(int hash, int length) {
        return Math.abs(hash % length);
    }

    public static int getRouteNodeIndex(String key, int nodeSize) {
        return indexFor(hash(key), nodeSize);
    }

    public static ACLEntity createACLEntity(HttpServletRequest req) {
        ACLEntity entity = new ACLEntity();
        entity.setClientIp(req.getRemoteAddr());
        entity.setHostIp(req.getLocalAddr());
        entity.setRequestMethod(req.getMethod());
        entity.setParameters(getRequestParams(req).toString());
        entity.setDatetime(new Date());
        log.info(ACLENTITY_ROW_CREATED);
        return entity;
    }

    public static String getRequestUrl(HttpServletRequest request) {
        String scheme = request.getScheme() + "://";
        String localAddr = request.getLocalAddr();
        String serverPort = (request.getServerPort() == 80) ? "" : ":" + request.getServerPort();
        String contextPath = request.getContextPath();
        String servletPath = request.getServletPath();
        return scheme + localAddr + serverPort + contextPath + servletPath;
    }

}
