package by.training.service;

import by.training.dao.JsonDataDao;
import by.training.exception.ClusterException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;


public class JsonDataService implements AutoCloseable {

    private static final Logger log = Logger.getLogger(JsonDataService.class);


    @Autowired
    private JsonDataDao jsonDataDao;


    public static JsonDataService newInstance() {
        return new JsonDataService();
    }


    public String getById(String id) throws ClusterException {
        return jsonDataDao.getById(id);
    }

    public String add(String id, String jsonStringData) throws ClusterException {
        return jsonDataDao.addOrUpdate(id, jsonStringData);
    }

    public String delete(String id) throws ClusterException {
        return jsonDataDao.delete(id);
    }

    public Map<String, String> getAll() throws ClusterException {
        return jsonDataDao.getAll();
    }

    @Override
    public void close() throws Exception {
        jsonDataDao.close();
        log.info("JsonDataDao close resources");
    }
}
