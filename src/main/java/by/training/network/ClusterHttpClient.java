package by.training.network;

import by.training.exception.ClusterException;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.*;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import static by.training.constants.Constants.*;

public final class ClusterHttpClient {

    private static final Logger log = Logger.getLogger(ClusterHttpClient.class);


    public static HttpResponse route(int currentNodeIndex, String method, Properties nodes, Map<String, Boolean> nodeStatuses, Map<String, String> parameters) throws ClusterException {
        HttpResponse routeResponse;
        String nodeRouteUrl = getRouteUrl(String.valueOf(currentNodeIndex), nodes, nodeStatuses);
        routeResponse = send(nodeRouteUrl, method, parameters);
        return routeResponse;
    }

    public static HttpResponse replicate(int currentNodeIndex, String currentNodeUrl, String method, Properties nodes, Map<String, Boolean> nodeStatuses, Map<String, String> parameters) throws ClusterException {
        HttpResponse response = null;
        for (Map.Entry entry : nodes.entrySet()) {
            if (currentNodeIndex == Integer.parseInt(entry.getValue().toString())) {
                String url = (String) entry.getKey();
                if (currentNodeUrl.equals(url) || !nodeStatuses.get(url)) {
                    continue;
                }
                response = send(url, method, parameters);
                if (HttpGet.METHOD_NAME.equals(method)) {
                    if (response != null && HttpStatus.SC_OK == response.getStatusLine().getStatusCode()) {
                        break;
                    }
                }
            }
        }
        return response;
    }

    public static HttpResponse send(String url, String method, Map<String, String> parameters) throws ClusterException {
        HttpResponse response;
        try {
            response = executeRequest(url, method, parameters);
        } catch (Exception e) {
            log.error(SEND + HTTP + method + FAILED, e);
            throw new ClusterException(SEND + HTTP + method + FAILED, e);
        }
        return response;
    }

    private static HttpResponse executeRequest(String url, String method, Map<String, String> parameters) throws ClusterException {
        CloseableHttpClient client = null;
        HttpRequestBase resendRequest = null;
        HttpResponse response = null;
        try {
            client = HttpClientBuilder.create().build();
            resendRequest = createRequest(url, method, parameters);

            response = client.execute(resendRequest);

        } catch (IOException e) {
            log.error(EXECUTE_REQUEST + FAILED, e);
            throw new ClusterException(EXECUTE_REQUEST + FAILED, e);
        } finally {
            if (resendRequest != null) {
                resendRequest.abort();
            }
            if (client != null) {
                try {
                    client.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return response;
    }

    private static HttpRequestBase createRequest(String url, String method, Map<String, String> parameters) throws ClusterException {
        HttpRequestBase request = null;
        try {
            if (method.equals(HttpPost.METHOD_NAME)) {
                HttpPost postRequest = new HttpPost(url);
                List<NameValuePair> urlParameters = new ArrayList<>();
                urlParameters.add(new BasicNameValuePair(ID, parameters.get(ID)));
                urlParameters.add(new BasicNameValuePair(JSON, parameters.get(JSON)));
                if (parameters.get(REPLICATE) != null) {
                    urlParameters.add(new BasicNameValuePair(REPLICATE, parameters.get(REPLICATE)));
                }
                if (parameters.get(SYNCHRO) != null) {
                    urlParameters.add(new BasicNameValuePair(SYNCHRO, parameters.get(SYNCHRO)));
                }
                postRequest.setEntity(new UrlEncodedFormEntity(urlParameters));
                request = postRequest;
                return request;
            }
            URIBuilder builder = new URIBuilder(url);
            for (Map.Entry<String, String> parameter : parameters.entrySet()) {
                builder.addParameter(parameter.getKey(), parameter.getValue());
            }
            URI resendUrl = builder.build();
            if (method.equals(HttpOptions.METHOD_NAME)) {
                request = new HttpOptions(resendUrl);
            }
            if (method.equals(HttpGet.METHOD_NAME)) {
                request = new HttpGet(resendUrl);
            }
            if (method.equals(HttpDelete.METHOD_NAME)) {
                request = new HttpDelete(resendUrl);
            }
        } catch (URISyntaxException | UnsupportedEncodingException e) {
            log.error(CREATE_REQUEST + FAILED, e);
            throw new ClusterException(CREATE_REQUEST + FAILED, e);
        }
        return request;
    }

    private static String getRouteUrl(String key, Properties nodes, Map<String, Boolean> nodeStatuses) throws ClusterException {
        String routeUrl = null;
        for (Map.Entry nodesEntry : nodes.entrySet()) {
            if (key.equals(nodesEntry.getValue())) {
                String url = (String) nodesEntry.getKey();
                if (nodeStatuses.get(url)) {
                    routeUrl = url;
                }
            }
        }
        if (routeUrl == null) {
            log.error(THE_SERVER_IS_TEMPORARILY_UNAVAILABLE_TRY_TO_QUERY_ANOTHER_ADDRESS);
            throw new ClusterException(THE_SERVER_IS_TEMPORARILY_UNAVAILABLE_TRY_TO_QUERY_ANOTHER_ADDRESS);
        }
        return routeUrl;
    }

    public static String getResponseData(HttpResponse httpResponse) throws ClusterException {
        BufferedReader rd = null;
        StringBuilder result;
        try {
            rd = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
            result = new StringBuilder();
            String line;
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
        } catch (IOException e) {
            log.error(GET_RESPONSE_DATA + FAILED, e);
            throw new ClusterException(GET_RESPONSE_DATA + FAILED, e);
        } finally {
            if (rd != null) {
                try {
                    rd.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return result.toString();
    }
}
