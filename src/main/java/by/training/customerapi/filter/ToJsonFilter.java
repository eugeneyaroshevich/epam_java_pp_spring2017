package by.training.customerapi.filter;

import by.training.parser.JsonParser;

import javax.servlet.*;
import java.io.IOException;

public class ToJsonFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        String jsonObject = JsonParser.toJsonFromRequest(servletRequest);
        servletRequest.setAttribute("jsonObject", jsonObject);

        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
    }
}
