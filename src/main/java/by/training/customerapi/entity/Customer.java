package by.training.customerapi.entity;

import by.training.reflection.b.Equal;
import by.training.reflection.b.Serialize;

@Serialize(alias = "by.training.customerapi.entity.Customer")
public class Customer implements Comparable<Customer> {

    @Equal
    @Serialize(alias = "id")
    private int id;

    @Equal
    @Serialize(alias = "name")
    private String name;

    @Equal
    @Serialize(alias = "surname")
    private String surname;

    @Equal
    @Serialize(alias = "age")
    private int age;


    public Customer() {
    }

    public Customer(int id, String name, String surname, int age) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }


    @Override
    public String toString() {
        return "id=" + id + ", name=" + name + ", surname=" + surname + ", age=" + age;
    }

    @Override
    public boolean equals(Object o) {


        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Customer customer = (Customer) o;

        if (id != customer.id) return false;
        if (age != customer.age) return false;
        if (name != null ? !name.equals(customer.name) : customer.name != null) return false;
        if (surname != null ? !surname.equals(customer.surname) : customer.surname != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + age;
        return result;
    }

    @Override
    public int compareTo(Customer o) {
        if (!this.equals(o)) {
            if (this.hashCode() != o.hashCode()) {
                return -1;
            }
        }
        return 0;
    }
}
