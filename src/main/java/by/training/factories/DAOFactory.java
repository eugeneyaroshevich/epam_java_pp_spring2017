package by.training.factories;

import by.training.dao.ACLEntityDao;
import by.training.dao.JsonDataDao;
import by.training.exception.ClusterException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

import static by.training.constants.Constants.DID_T_CLOSE_CONNECTION;

public class DAOFactory {

    private static final Logger log = Logger.getLogger(DAOFactory.class);


    @Autowired
    private DataSource dataSource;


    public static DAOFactory newInstance() throws ClusterException {
        return new DAOFactory();
    }


    public ACLEntityDao getACLEntityDao() {
        return ACLEntityDao.newInstance();
    }

    public JsonDataDao getJsonDataDao() throws ClusterException {
        return JsonDataDao.newInstance(getConnection(dataSource));
    }

    private Connection getConnection(DataSource dataSource) throws ClusterException {
        log.trace("open connection");
        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            log.error(DID_T_CLOSE_CONNECTION,e);
            throw new ClusterException(DID_T_CLOSE_CONNECTION, e);
        }
    }
}
