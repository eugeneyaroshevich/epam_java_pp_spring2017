package by.training.factories;

import by.training.cache.Cache;
import by.training.cache.ConcurrentLFUCache;
import by.training.cache.ConcurrentLRUCache;
import by.training.exception.ClusterException;
import org.apache.log4j.Logger;

import java.util.Properties;

import static by.training.constants.Constants.*;
import static by.training.loader.ClusterLoader.loadProperties;

public class CacheFactory {

    private static final Logger log = Logger.getLogger(CacheFactory.class);

    private Properties properties;
    private String typeCache;
    private int size;
    private float loadfactor;

    private CacheFactory() throws ClusterException {
        setProperties(loadProperties(CACHE_PROPERTIES));
        setTypeCache(properties.getProperty(CACHE));
        setSize(Integer.parseInt(properties.getProperty(SIZE)));
        setLoadfactor(Float.parseFloat(properties.getProperty(LOADFACTOR)));
    }


    public static CacheFactory newInstance() throws ClusterException {
        return new CacheFactory();
    }


    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    private String getTypeCache() {
        return typeCache;
    }

    private void setTypeCache(String typeCache) {
        this.typeCache = typeCache;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    private float getLoadfactor() {
        return loadfactor;
    }

    private void setLoadfactor(float loadfactor) {
        this.loadfactor = loadfactor;
    }

    public <K, V> Cache<K, V> getCache() {
        if (LFU.equals(getTypeCache())) {
            return ConcurrentLFUCache.newInstance();
        } else if (LRU.equals(getTypeCache())) {
            return ConcurrentLRUCache.newInstance(getSize(), getLoadfactor());
        }
        log.info("cache initialized");
        return null;
    }
}
