package by.training.exception;

public class ClusterException extends Exception {

    public ClusterException() {
    }

    public ClusterException(String message, Throwable cause) {
        super(message, cause);
    }

    public ClusterException(String message) {
        super(message);
    }
}
