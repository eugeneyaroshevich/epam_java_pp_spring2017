package junit.reflection.b;

import by.training.reflection.b.Equal;
import by.training.reflection.b.Serialize;

@Serialize(alias = "")
public class TestBeanIfNotMarkedSerializeFieldOfBean implements Comparable<TestBeanIfNotMarkedSerializeFieldOfBean> {

//    @Equal
//    @Serialize(alias = "name")
    private String name;

    @Equal
    @Serialize(alias = "surname")
    private String surname;

    @Equal
    @Serialize(alias = "age")
    private int age;

    @Equal
    @Serialize(alias = "bean")
    private TestBeanIfNotMarkedSerializeFieldOfBean bean;

    public TestBeanIfNotMarkedSerializeFieldOfBean() {
    }

    public TestBeanIfNotMarkedSerializeFieldOfBean(String name, String surname, int age, TestBeanIfNotMarkedSerializeFieldOfBean bean) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.bean = bean;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public TestBeanIfNotMarkedSerializeFieldOfBean getBean() {
        return bean;
    }

    public void setBean(TestBeanIfNotMarkedSerializeFieldOfBean bean) {
        this.bean = bean;
    }

    @Override
    public String toString() {
        return "name=" + name + ", surname=" + surname + ", age=" + age + ", bean=" + bean;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TestBeanIfNotMarkedSerializeFieldOfBean bean1 = (TestBeanIfNotMarkedSerializeFieldOfBean) o;

        if (age != bean1.age) return false;
        if (name != null ? !name.equals(bean1.name) : bean1.name != null) return false;
        if (surname != null ? !surname.equals(bean1.surname) : bean1.surname != null) return false;
        return bean != null ? bean.equals(bean1.bean) : bean1.bean == null;

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + age;
        result = 31 * result + (bean != null ? bean.hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(TestBeanIfNotMarkedSerializeFieldOfBean o) {
        if (!this.equals(o)) {
            if (this.hashCode() != o.hashCode()) {
                return -1;
            }
        }
        return 0;
    }
}
