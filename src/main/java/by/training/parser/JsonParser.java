package by.training.parser;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import javax.servlet.ServletRequest;
import java.util.Enumeration;


public final class JsonParser {

    private final static Gson gson = new Gson();

    public static String toJson(Object object) {

        return gson.toJson(object);
    }

    public static <T> T fromJson(JsonObject jsonObject, Class<T> clazz) {

        return gson.fromJson(jsonObject, clazz);
    }

    public static <T> T fromJson(String jsonObject, Class<T> clazz) {

        return gson.fromJson(jsonObject, clazz);
    }

    public static int toIdFromJsonCustomer(String jsonObject) {

        JsonObject customerId = gson.fromJson(jsonObject, JsonObject.class);
        JsonElement id = customerId.get("id");

        return id.getAsInt();
    }

    public static String toIdFromJson(String jsonObject) {

        JsonObject customerId = gson.fromJson(jsonObject, JsonObject.class);
        JsonElement id = customerId.get("id");

        return id.getAsString();
    }

    public static String toJsonFromRequestCustomer(ServletRequest req) {

        JsonObject jsonObject = new JsonObject();

        Enumeration parameterNames = req.getParameterNames();
        while (parameterNames.hasMoreElements()) {
            String key = (String) parameterNames.nextElement();

            if ("action".equals(key) || "cache".equals(key)) {
                continue;
            }

            String value = req.getParameter(key);

            jsonObject.addProperty(key, value);

        }
        return jsonObject.toString();
    }

    public static String toJsonFromRequest(ServletRequest req) {

        JsonObject jsonObject = new JsonObject();

        Enumeration parameterNames = req.getParameterNames();
        while (parameterNames.hasMoreElements()) {
            String key = (String) parameterNames.nextElement();

//            if ("id".equals(key)) {
//                continue;
//            }

            String value = req.getParameter(key);

            jsonObject.addProperty(key, value);

        }
        return jsonObject.toString();
    }

}
