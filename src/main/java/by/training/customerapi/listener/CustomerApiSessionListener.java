package by.training.customerapi.listener;



import by.training.customerapi.service.CustomerServiceImpl;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;


public class CustomerApiSessionListener implements HttpSessionListener {

    private ServletContext servletContext;
    private HttpSession httpSession;

    private CustomerServiceImpl lfuCustomerService;
    private CustomerServiceImpl lruCustomerService;

    @Override
    public void sessionCreated(HttpSessionEvent httpSessionEvent) {

        httpSession = httpSessionEvent.getSession();
        servletContext = httpSession.getServletContext();

        lfuCustomerService = (CustomerServiceImpl) servletContext.getAttribute("lfuCustomerService");
        lruCustomerService = (CustomerServiceImpl) servletContext.getAttribute("lruCustomerService");

        httpSession.setAttribute("lfuCustomerService", lfuCustomerService);
        httpSession.setAttribute("lruCustomerService", lruCustomerService);


    }

    @Override
    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
        httpSession.removeAttribute("lfuCustomerService");
        httpSession.removeAttribute("lruCustomerService");
    }
}
