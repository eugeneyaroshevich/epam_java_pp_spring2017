package by.training.util;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


import static by.training.constants.Constants.*;


public class HibernateUtil {

    private static final SessionFactory sessionFactory;

    private static final Logger log = Logger.getLogger(HibernateUtil.class);

    static {
        try {
            sessionFactory = new Configuration().configure().buildSessionFactory();
        } catch (Throwable ex) {
            log.error(INITIAL_SESSION_FACTORY_CREATION_FAILED, ex);
            throw new ExceptionInInitializerError(ex);
        }

        log.info("Hibernate SessionFactory created");
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public static void shutdown() {
        log.trace("closing session");
        sessionFactory.getCurrentSession().close();

        log.trace("closing Hibernate SessionFactory");
        getSessionFactory().close();

        log.info("Hibernate SessionFactory destructed");
    }
}
