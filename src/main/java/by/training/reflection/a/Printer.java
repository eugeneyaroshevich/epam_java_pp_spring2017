package by.training.reflection.a;

@Proxy
public class Printer implements Printable {

    @Override
    public String print(String string){
        System.out.println(string);
        return string;
    }
}
