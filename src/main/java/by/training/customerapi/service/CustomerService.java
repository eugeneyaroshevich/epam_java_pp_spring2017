package by.training.customerapi.service;

import by.training.customerapi.entity.Customer;
import by.training.exception.ClusterException;

public interface CustomerService {

    void add(Customer customer) throws ClusterException;

    Customer getById(int id) throws ClusterException;

}
