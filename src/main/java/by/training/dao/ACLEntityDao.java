package by.training.dao;

import by.training.entity.ACLEntity;
import by.training.util.ClusterUtil;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;


public class ACLEntityDao implements AutoCloseable{

    private static final Logger log = Logger.getLogger(ACLEntityDao.class);


    @Autowired
    private SessionFactory sessionFactory;

    public static ACLEntityDao newInstance() {
        return new ACLEntityDao();
    }

    @Transactional
    public void log(ACLEntity entity) {
        log.info("adding a new ACL log record where id:" + entity.getId());
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.save(entity);
            transaction.commit();
        } catch (HibernateException e) {
            assert transaction != null;
            transaction.rollback();
            log.warn("can't add new ACL log record where id:" + entity.getId(), e);
        }
//        Not necessarily in the hibernate.current_session_context_class mode

//        finally {
//            sessionFactory.getCurrentSession().close();
//        }

        log.info("new ACL log record added where id:" + entity.getId());
    }

    @Override
    public void close() {
        log.trace("closing session");
        sessionFactory.getCurrentSession().close();

        log.trace("closing Hibernate SessionFactory");
        sessionFactory.close();

        log.info("ACLEntityDao close");
    }
}
