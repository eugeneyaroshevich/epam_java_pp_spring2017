package by.training.validator;

public class HelloWorld {
    public static void main(String[] args) {
        String msg = "Hello World! ";
        if (args.length == 1) {
            msg += args[0];
        }
        System.out.println(msg);
    }
}