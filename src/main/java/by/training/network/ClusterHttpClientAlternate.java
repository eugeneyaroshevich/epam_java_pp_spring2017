package by.training.network;

import by.training.exception.ClusterException;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpOptions;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static by.training.constants.Constants.FAILED;
import static by.training.constants.Constants.HTTP;

public class ClusterHttpClientAlternate {

    private static final Logger log = Logger.getLogger(ClusterHttpClientAlternate.class);


    public static ClusterHttpClientAlternate newInstance() {
        return new ClusterHttpClientAlternate();
    }


    public HttpResponse get(String url, Map<String, String> parameters) throws ClusterException {
        CloseableHttpClient client = null;
        HttpGet httpGet = null;
        HttpResponse response;
        try {
            client = HttpClientBuilder.create().build();
            URIBuilder builder = new URIBuilder(url);
            for (Map.Entry<String, String> parameter : parameters.entrySet()) {
                builder.addParameter(parameter.getKey(), parameter.getValue());
            }
            URI sendUrl = builder.build();
            httpGet = new HttpGet(sendUrl.toString());

            response = client.execute(httpGet);

        } catch (IOException | URISyntaxException e) {
            log.error(HTTP + HttpGet.METHOD_NAME + FAILED, e);
            throw new ClusterException(HTTP + HttpGet.METHOD_NAME + FAILED);
        } finally {
            if (httpGet != null) {
                httpGet.abort();
            }
            if (client != null) {
                try {
                    client.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return response;
    }

    public HttpResponse put(String url, Map<String, String> parameters) throws ClusterException {
        CloseableHttpClient client = null;
        HttpPost httpPost = null;
        HttpResponse response;
        try {
            client = HttpClientBuilder.create().build();
            URIBuilder builder = new URIBuilder(url);
            List<NameValuePair> urlParameters = new ArrayList<>();
            for (Map.Entry<String, String> parameter : parameters.entrySet()) {
                urlParameters.add(new BasicNameValuePair(parameter.getKey(), parameter.getValue()));
            }
            builder.addParameters(urlParameters);
            URI sendUrl = builder.build();
            httpPost = new HttpPost(sendUrl.toString());

            response = client.execute(httpPost);

        } catch (IOException | URISyntaxException e) {
            log.error(HTTP + HttpPost.METHOD_NAME + FAILED, e);
            throw new ClusterException(HTTP + HttpPost.METHOD_NAME + FAILED);
        } finally {
            if (httpPost != null) {
                httpPost.abort();
            }
            if (client != null) {
                try {
                    client.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return response;
    }

    public HttpResponse delete(String url, Map<String, String> parameters) throws ClusterException {
        CloseableHttpClient client = null;
        HttpDelete httpDelete = null;
        HttpResponse response;
        try {
            client = HttpClientBuilder.create().build();
            URIBuilder builder = new URIBuilder(url);
            for (Map.Entry<String, String> parameter : parameters.entrySet()) {
                builder.addParameter(parameter.getKey(), parameter.getValue());
            }
            URI sendUrl = builder.build();
            httpDelete = new HttpDelete(sendUrl.toString());

            response = client.execute(httpDelete);

        } catch (IOException | URISyntaxException e) {
            log.error(HTTP + HttpDelete.METHOD_NAME + FAILED, e);
            throw new ClusterException(HTTP + HttpDelete.METHOD_NAME + FAILED);
        } finally {
            if (httpDelete != null) {
                httpDelete.abort();
            }
            if (client != null) {
                try {
                    client.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return response;
    }

    public HttpResponse options(String url, Map<String, String> parameters) throws ClusterException {
        CloseableHttpClient client = null;
        HttpOptions httpOptions = null;
        HttpResponse response;
        try {
            client = HttpClientBuilder.create().build();
            URIBuilder builder = new URIBuilder(url);
            for (Map.Entry<String, String> parameter : parameters.entrySet()) {
                builder.addParameter(parameter.getKey(), parameter.getValue());
            }
            URI sendUrl = builder.build();
            httpOptions = new HttpOptions(sendUrl.toString());

            response = client.execute(httpOptions);

        } catch (IOException | URISyntaxException e) {
            log.error(HTTP + HttpGet.METHOD_NAME + FAILED, e);
            throw new ClusterException(HTTP + HttpGet.METHOD_NAME + FAILED);
        } finally {
            if (httpOptions != null) {
                httpOptions.abort();
            }
            if (client != null) {
                try {
                    client.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return response;
    }
}
