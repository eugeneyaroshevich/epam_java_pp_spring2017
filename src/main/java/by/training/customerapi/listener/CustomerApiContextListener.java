package by.training.customerapi.listener;

import by.training.cache.ConcurrentLFUCache;
import by.training.cache.ConcurrentLRUCache;
import by.training.customerapi.dao.CustomerDao;
import by.training.customerapi.dao.CustomerDaoImpl;
import by.training.reflection.a.ProxyFactory;
import by.training.customerapi.service.CustomerServiceImpl;

import javax.servlet.*;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

public class CustomerApiContextListener implements ServletContextListener {

    private ServletContext servletContext;

    private ConcurrentLFUCache<String, Map<String, Object>> lfuCache;
    private ConcurrentLRUCache<String, Map<String, Object>> lruCache;

    private CustomerDao lfuCustomerDaoImpl;
    private CustomerDao lruCustomerDaoImpl;

    private CustomerServiceImpl lfuCustomerService;
    private CustomerServiceImpl lruCustomerService;

    public void contextInitialized(ServletContextEvent event) {

        try {

            servletContext = event.getServletContext();

            final int lfuInitSize = Integer.parseInt(event.getServletContext().getInitParameter("lfu_init_size"));
            final int lruInitSize = Integer.parseInt(event.getServletContext().getInitParameter("lru_init_size"));

            lfuCache = new ConcurrentLFUCache<>(lfuInitSize);
            lruCache = new ConcurrentLRUCache<>(lruInitSize, 0.75f);

            servletContext.setAttribute("lfuCache", lfuCache);
            servletContext.setAttribute("lruCache", lruCache);


            lfuCustomerDaoImpl = (CustomerDao) ProxyFactory.getInstanceOf(CustomerDaoImpl.class, lfuCache);
            servletContext.setAttribute("lfuCustomerDaoImpl", lfuCustomerDaoImpl);

            lruCustomerDaoImpl = (CustomerDao) ProxyFactory.getInstanceOf(CustomerDaoImpl.class, lruCache);
            servletContext.setAttribute("lruCustomerDaoImpl", lruCustomerDaoImpl);

            lfuCustomerService = new CustomerServiceImpl();
            lfuCustomerService.setCustomerDao(lfuCustomerDaoImpl);
            servletContext.setAttribute("lfuCustomerService", lfuCustomerService);

            lruCustomerService = new CustomerServiceImpl();
            lruCustomerService.setCustomerDao(lruCustomerDaoImpl);
            servletContext.setAttribute("lruCustomerService", lruCustomerService);

        } catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public void contextDestroyed(ServletContextEvent arg0) {
    }
}
