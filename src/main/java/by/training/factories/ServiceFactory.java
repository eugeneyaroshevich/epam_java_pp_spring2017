package by.training.factories;

import by.training.service.JsonDataService;

public class ServiceFactory {

    public static ServiceFactory newInstance() {
        return new ServiceFactory();
    }

    public static JsonDataService getJsonDataService() {
        return JsonDataService.newInstance();
    }
}
