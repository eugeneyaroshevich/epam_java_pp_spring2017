package by.training.dao;

import by.training.cache.Cache;
import by.training.exception.ClusterException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;

import static by.training.constants.Constants.*;


public class JsonDataDao {

    private static final Logger log = Logger.getLogger(JsonDataDao.class);

    @Autowired
    private Cache<String, String> cache;
    private final Connection connection;
    private final Map<String, PreparedStatement> preparedStatements = new HashMap<>();
    private PreparedStatement statement;
    private ResultSet resultSet;

    public JsonDataDao(Connection connection) throws ClusterException {
        this.connection = connection;
        createTable();
    }


    public static JsonDataDao newInstance(Connection connection) throws ClusterException {
        return new JsonDataDao(connection);
    }


    public Map<String, String> getAll() throws ClusterException {

        Map<String, String> result = new HashMap<>();

        try {
            statement = getPreparedStatement(GET_ALL_JSONDATA);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.put(resultSet.getString("id"), resultSet.getString("jsondata"));
            }

        } catch (SQLException e) {
            log.warn("get all failed! ",e);
            throw new ClusterException("get all failed! ", e);
        }
        return result;
    }

    public String getById(String id) throws ClusterException {

        log.info("get data by id: " + id);

        try {
            log.trace("get data from cache by id: " + id);
            String value = cache.get(id);
            if (value != null) {
                log.info("returned from cache data: " + value + " where id: " + id);
                return value;
            }

            statement = getPreparedStatement(GET_JSONDATA_BY_ID);
            statement.setString(ONE, id);

            log.trace("getiing result set of data from database");
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                value = resultSet.getString("jsondata");
                cache.put(id, value);
                log.trace("data from database put in cache");

                log.info("returned from database data: " + value + " where id: " + id);
                return value;
            } else {
                log.warn("there is no data with this id");
                throw new ClusterException(THERE_IS_NO_DATA_WITH_THIS_ID);
            }

        } catch (SQLException e) {
            log.warn("get by id failed", e);
            throw new ClusterException("get by id failed", e);
        }
    }

    public String addOrUpdate(String id, String jsonString) throws ClusterException {

        log.info("add or update data: " + jsonString + " where id: " + id);

        try {
            statement = getPreparedStatement(ADD_OR_UPDATE_JSONDATA);
            statement.setString(ONE, id);
            statement.setString(TWO, jsonString);
            statement.executeUpdate();
            log.trace("data: " + jsonString + " where id: " + id + " added in database");

            cache.put(id, jsonString);
            log.trace("data: " + jsonString + " where id: " + id + " put in cache");

            return id;
        } catch (SQLException e) {
            log.warn("addOrUpdate failed", e);
            throw new ClusterException(ADD_OR_UPDATE_FAILED, e);
        }
    }

    public String delete(String id) throws ClusterException {
        log.info("delete data where id: " + id);
        try {
            statement = getPreparedStatement(DELETE_JSONDATA);
            statement.setString(ONE, id);

            int count = statement.executeUpdate();
            if (count > ONE) {
                log.warn("delete more then 1 record: " + count);
                throw new ClusterException(DELETE_MORE_THEN_1_RECORD + count);
            }
            log.trace("removed from database data where id: " + id);

            cache.remove(id);
            log.trace("removed from cache data where id: " + id);

            return id;
        } catch (SQLException e) {
            log.warn("delete failed! ", e);
            throw new ClusterException("delete failed! ", e);
        }
    }


    private void createTable() throws ClusterException {
        log.info("create table jsondata");
        try {
            statement = getPreparedStatement(CREATE_TABLE_JSONDATA);
            statement.executeUpdate();
        } catch (SQLException e) {
            log.warn("create table failed!");
            throw new ClusterException(CREATE_TABLE_FAILED,e);
        }
    }

    private PreparedStatement getPreparedStatement(String sql) throws ClusterException {
        log.info("create preparedStatement: " + sql);
        PreparedStatement preparedStatement = preparedStatements.get(sql);
        if (preparedStatement == null) {
            try {
                preparedStatement = connection.prepareStatement(sql);
                preparedStatements.put(sql, preparedStatement);
                log.trace("preparedStatement: " + sql + "initialised");
            } catch (SQLException e) {
                log.warn("get preparedStatement failed", e);
                throw new ClusterException(GET_PREPARED_STATEMENT_FAILED, e);
            }
        }
        log.info("preparedStatement created: " + sql);
        return preparedStatement;
    }

    public void close() throws ClusterException {
        log.info("closing JsonDataDao resources");
        resultsetClose();
        preparedStatementClose();
        connectionClose();
        log.info("JsonDataDao resources closed");
    }

    private void resultsetClose() throws ClusterException {
        if (resultSet != null) {
            try {
                resultSet.close();
                log.trace("result set closed");
            } catch (SQLException e) {
                log.warn("can't close resultset", e);
                throw new ClusterException(DID_T_CLOSE_RESULTSET, e);
            }
        }
    }

    private void preparedStatementClose() throws ClusterException {
        for (PreparedStatement preparedStatement : preparedStatements.values()) {
            try {
                if (preparedStatement != null) {
                    preparedStatement.close();
                    log.trace("prepared statement closed");
                }
            } catch (SQLException e) {
                log.warn("did't close prepared statement", e);
                throw new ClusterException(DID_T_CLOSE_PREPARED_STATEMENT, e);
            }
        }
    }

    private void connectionClose() throws ClusterException {
        if (connection != null) {
            try {
                connection.close();
                log.trace("connection close");
            } catch (SQLException e) {
                log.warn("can't close connection", e);
                throw new ClusterException(DID_T_CLOSE_CONNECTION, e);
            }
        }
    }
}
