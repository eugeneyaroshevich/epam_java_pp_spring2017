package testng;

import by.training.cache.Cache;
import by.training.cache.ConcurrentLFUCache;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


public class ConcurrentLFUCacheTest extends Assert {

    private Cache<Integer, Integer> concurrentLFUCache;

    @BeforeMethod
    public void before() {
        concurrentLFUCache = new ConcurrentLFUCache<>();
    }

    @AfterMethod
    public void after() {
        concurrentLFUCache = null;
    }


    //newInstance()
    @Test(groups = "newInstance")
    public void testCreateNewInstanceNotNullReturned(){
        assertNotNull(ConcurrentLFUCache.newInstance());
    }

    @Test(groups = "newInstance")
    public void testCreateNewInstanceWithCapacityConstructorNotNullReturned(){
        assertNotNull(ConcurrentLFUCache.newInstance(7));
    }

    // size() / put()
    @Test(groups = "size", dependsOnMethods = "testCreateNewInstanceNotNullReturned")
    public void testSizeReturnSizeEqualsCapacity() {
        concurrentLFUCache.put(0, 0);
        concurrentLFUCache.put(1, 1);
        concurrentLFUCache.put(2, 2);
        concurrentLFUCache.put(3, 3);

        assertEquals(concurrentLFUCache.size(), 3);
    }

    @Test(groups = "size", dependsOnGroups = "newInstance")
    public void testSizeOwerwritingValuesWithTheSameKeysTwoReturned() {

        concurrentLFUCache.put(0, 0);
        concurrentLFUCache.put(1, 1);
        concurrentLFUCache.put(1, 2);

        assertEquals(concurrentLFUCache.size(), 2);
    }


    // test put()
    @Test(groups = "put")
    public void testPutRemoveEldestEntryGetEldestKeyNullReturned() {
        concurrentLFUCache.put(0, 0);
        concurrentLFUCache.put(1, 1);
        concurrentLFUCache.put(2, 2);
        concurrentLFUCache.put(3, 3);

        assertNull(concurrentLFUCache.get(0));
    }

    @Test(groups = "put")
    public void testPutRemoveEldestEntryGetYoungestKeyNotNullReturned() {
        concurrentLFUCache.put(0, 0);
        concurrentLFUCache.put(1, 1);
        concurrentLFUCache.put(2, 2);
        concurrentLFUCache.put(3, 3);

        assertNotNull(concurrentLFUCache.get(1));
        assertNotNull(concurrentLFUCache.get(2));
        assertNotNull(concurrentLFUCache.get(3));
    }

    @Test(groups = "put")
    public void testPutRemoveEldestEntryGetYoungestValuesReturnedEquals(){
        concurrentLFUCache.put(0, 0);
        concurrentLFUCache.put(1, 1);
        concurrentLFUCache.get(1);
        concurrentLFUCache.put(2, 2);
        concurrentLFUCache.get(2);
        concurrentLFUCache.put(3, 3);
        concurrentLFUCache.put(4, 4);
        concurrentLFUCache.get(3);
        concurrentLFUCache.put(5, 5);
        concurrentLFUCache.put(6, 6);
        concurrentLFUCache.get(6);
        concurrentLFUCache.put(7, 7); // {key=2, value=2, key=6, value=6, key=7, value=7}

        assertEquals(concurrentLFUCache.get(2),new Integer(2));
        assertEquals(concurrentLFUCache.get(6),new Integer(6));
        assertEquals(concurrentLFUCache.get(7),new Integer(7));
    }


    // get()
    @Test(groups = "get")
    public void testGetNotContainKeyNullReturned() {

        assertEquals(concurrentLFUCache.size(), 0);

        assertNull(concurrentLFUCache.get(0));
        assertNull(concurrentLFUCache.get(5));
        assertNull(concurrentLFUCache.get(7));
    }

    @Test(groups = "get")
    public void testGetContainKeyNotNullReturned() {

        concurrentLFUCache.put(0, 0);
        concurrentLFUCache.put(2, 2);

        assertNotNull(concurrentLFUCache.get(0));
        assertNotNull(concurrentLFUCache.get(2));
    }

    @Test(groups = "get")
    public void testGetPutKeyValueReturnThisValueForThisKey() {

        concurrentLFUCache.put(0, 0);
        concurrentLFUCache.put(7, 7);

        assertEquals(concurrentLFUCache.get(0), new Integer(0));
        assertEquals(concurrentLFUCache.get(7), new Integer(7));

    }

    @Test(groups = "get")
    public void testGetPutKeyValueReturnSameValueForThisKey() {

        Integer sameInteger = 0;
        concurrentLFUCache.put(0, sameInteger);

        assertSame(concurrentLFUCache.get(0), sameInteger);
    }

    @Test(groups = "get")
    public void testGetPutKeyValueReturnNotSameValueForThisKey() {

        Integer sameInteger = 0;
        concurrentLFUCache.put(0, sameInteger);

        Integer notSameInteger = 1;

        assertNotSame(concurrentLFUCache.get(0), notSameInteger);
    }

}
