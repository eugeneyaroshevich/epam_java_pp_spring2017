package junit.constants;

import by.training.constants.Constants;
import org.junit.Test;

import static org.junit.Assert.*;

public class ConstantsTest {

    @Test
    public void testCreateNewInstanceConstantClass(){
        Constants constants = new Constants();

        assertNotNull(constants);
    }

    @Test
    public void testDefaultLoadFactorFieldNotNullReturned(){

        assertNotNull(Constants.DEFAULT_LOADFACTOR);
    }

    @Test
    public void testDefaultCapacityEqualsThree(){

        assertEquals(3,Constants.DEFAULT_CAPACITY);
    }
}