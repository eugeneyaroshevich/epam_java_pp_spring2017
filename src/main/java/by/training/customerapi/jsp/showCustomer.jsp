<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Show Customer by ID</title>
</head>
<body>
<form method="GET" action='customer' name="frmShowCustomer"><input
        type="hidden" name="action" value="getCustomer"/>
    <p><b>Get Customer by ID</b></p>
    <table>
        <tr>
            <td>Type of cache</td>
            <td><input type="text" name="cache"/></td>
        </tr>
        <tr>
            <td>Customer ID</td>
            <td><input type="text" name="id"/></td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" value="Get Customer"/></td>
        </tr>
    </table>
</form>
<p><a href="/by/training/customerapi/jsp/createCustomer.jsp">Add Customer</a></p>
</body>
</html>
