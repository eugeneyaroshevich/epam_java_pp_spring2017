package by.training.configuration;

import by.training.cache.Cache;
import by.training.context.ClusterContext;
import by.training.dao.ACLEntityDao;
import by.training.dao.JsonDataDao;
import by.training.exception.ClusterException;
import by.training.factories.CacheFactory;
import by.training.factories.DAOFactory;
import by.training.factories.ServiceFactory;
import by.training.service.JsonDataService;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.servlet.ServletContext;
import javax.sql.DataSource;
import java.util.Properties;

import static by.training.constants.Constants.*;


@Configuration
@ComponentScan(BY_TRAINING)
@PropertySource(value = {CLASSPATH_APP_PROPERTIES})
public class ClusterConfig {

    private static final Logger log = Logger.getLogger(ClusterConfig.class);

    @Autowired
    private Environment env;

    @Autowired
    private ServletContext context;


    @Bean
    public ClusterContext clusterContext() throws ClusterException {
        return ClusterContext.build(context.getContextPath());
    }

    @Bean
    public Cache<String, String> cache() throws ClusterException {
        return CacheFactory.newInstance().getCache();
    }

    @Bean
    @Primary
    public DataSource dataSource() {
        return DataSourceBuilder
                .create()
                .driverClassName(env.getRequiredProperty(JDBC_DRIVER))
                .url(env.getRequiredProperty(JDBC_URL)+ context.getContextPath())
                .username(env.getRequiredProperty(JDBC_USERNAME))
                .password(env.getRequiredProperty(JDBC_PASSWORD))
                .build();
    }

    @Bean
    public LocalSessionFactoryBean sessionFactory() {
        LocalSessionFactoryBean factoryBean = new LocalSessionFactoryBean();
        factoryBean.setDataSource(dataSource());
        factoryBean.setHibernateProperties(hibernateProperties());
        factoryBean.setAnnotatedClasses(by.training.entity.ACLEntity.class);
        return factoryBean;
    }

    private Properties hibernateProperties() {
        Properties properties = new Properties();
        properties.put(HIBERNATE_DIALECT, env.getRequiredProperty(HIBERNATE_DIALECT));
        properties.put(HIBERNATE_HBM2DDL_AUTO, env.getRequiredProperty(HIBERNATE_HBM2DDL_AUTO));
        properties.put(HIBERNATE_CONNECTION_POOL_SIZE, env.getRequiredProperty(HIBERNATE_CONNECTION_POOL_SIZE));
        properties.put(HIBERNATE_CURRENT_SESSION_CONTEXT_CLASS, THREAD);
        properties.put(HIBERNATE_SHOW_SQL, env.getRequiredProperty(HIBERNATE_SHOW_SQL));
        return properties;
    }

    @Bean
    public HibernateTransactionManager transactionManager(SessionFactory sessionFactory) {
        HibernateTransactionManager txManager = new HibernateTransactionManager();
        txManager.setSessionFactory(sessionFactory);
        return txManager;
    }

    @Bean
    public DAOFactory daoFactory() throws ClusterException {
        return DAOFactory.newInstance();
    }

    @Bean
    public ServiceFactory serviceFactory() {
        return ServiceFactory.newInstance();
    }

    @Bean
    public JsonDataService jsonDataService() {
        return ServiceFactory.getJsonDataService();
    }

    @Bean
    public ACLEntityDao aclEntityDao() throws ClusterException {
        return daoFactory().getACLEntityDao();
    }

    @Bean
    public JsonDataDao jsonDataDao() throws ClusterException {
        return daoFactory().getJsonDataDao();
    }

}
