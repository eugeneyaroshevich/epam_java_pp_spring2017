//package by.training.customerapi.test.beans;
//
//import by.training.customerapi.entity.Customer;
//import org.junit.Before;
//import org.junit.Test;
//
//import static org.junit.Assert.*;
//
//public class CustomerTest {
//
//    private Customer customer;
//    private Customer compareCustomer;
//    private Customer notCompareCustomer;
//    private String toStringOfCustomer;
//    private int hashCodeOfCustomer;
//
//    @Before
//    public void setUp() throws Exception {
//        customer = new Customer(0, "Bean1", "Binich1", 40);
//        compareCustomer = new Customer(0, "Bean1", "Binich1", 40);
//        notCompareCustomer = new Customer(1, "Bean2", "Binich2", 41);
//        toStringOfCustomer = "id=0, name=Bean1, surname=Binich1, age=40";
//        hashCodeOfCustomer = -1855618369;
//    }
//
//    @Test
//    public void testToString() throws Exception {
//
//        assertNotNull(customer.toString());
//        assertEquals(customer.toString(),toStringOfCustomer);
//    }
//
//    @Test
//    public void testHashCode() throws Exception {
//
//        assertNotNull(customer.hashCode());
//        assertEquals(customer.hashCode(),hashCodeOfCustomer);
//
//    }
//
//    @Test
//    public void testCompareTo(){
//
//        assertEquals(customer.compareTo(compareCustomer),0);
//        assertEquals(customer.compareTo(notCompareCustomer),-1);
//
//    }
//
//}