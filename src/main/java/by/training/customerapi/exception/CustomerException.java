package by.training.customerapi.exception;

public class CustomerException extends Exception {

    public CustomerException() {
    }

    public CustomerException(String message, Throwable cause) {
        super(message, cause);
    }

    public CustomerException(String message) {
        super(message);
    }
}
