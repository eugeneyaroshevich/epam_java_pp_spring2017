package by.training.customerapi.dao;

import by.training.customerapi.entity.Customer;
import by.training.exception.ClusterException;
import by.training.reflection.a.Proxy;

@Proxy(invocationHandler = "by.training.customerapi.dao.CustomerDaoImplProxyHandler")
public class CustomerDaoImpl implements CustomerDao {

    public CustomerDaoImpl() {
    }

    @Override
    public void add(Customer customer) throws ClusterException {
        // go to BD
        System.out.println("go to bd addOrUpdate");
        System.out.println("added: " + customer);
    }

    @Override
    public Customer getById(int id) throws ClusterException {
        // go to BD
        System.out.println("go to bd getById");
        System.out.println("get from bd customer by id: " + id);
        return new Customer(id, "fromBaseCustomer", "fromBase", 1);
    }

}
