package by.training.constants;

public final class Constants {

    public static final int ZERO = 0;
    public static final int ONE = 1;
    public static final int TWO = ONE + ONE;
    public static final int THREE = TWO + ONE;

    public static final int DEFAULT_CAPACITY = THREE;
    public static final float DEFAULT_LOADFACTOR = 0.75f;

    public static final String APPLICATION_JSON = "application/json";
    public static final String UTF_8 = "UTF-8";

    public static final String GET_ALL_JSONDATA = "SELECT * FROM jsondata";
    public static final String GET_JSONDATA_BY_ID = "SELECT jsondata FROM jsondata WHERE id=?";
    //    public static final String ADD_OR_UPDATE_JSONDATA = "INSERT INTO jsondata (id, jsondata) VALUES( ?,  ?) ON DUPLICATE KEY UPDATE jsondata = ?";
    public static final String ADD_OR_UPDATE_JSONDATA = "MERGE INTO jsondata (id, jsondata) VALUES( ?,  ?)";
    public static final String DELETE_JSONDATA = "DELETE FROM jsondata WHERE id= ?";
    public static final String CREATE_TABLE_JSONDATA = "CREATE TABLE IF NOT EXISTS `jsondata` (" +
            "  `id` VARCHAR(45) NOT NULL,\n" +
            "  `jsondata` LONGTEXT NOT NULL,\n" +
            "  PRIMARY KEY (`id`));";

    public static final String NODE_PROPERTIES = "node.properties";
    public static final String SET_NODE_STATUSES_NOT_FINISHED = "set node statuses not finished";
    public static final String CLUSTER_STATUS = "clusterStatus";
    public static final String PLEASE_WAIT_CLUSTER_DON_T_READY_WORK = "Please wait! Cluster don't ready work.";
    public static final String THE_SERVER_IS_TEMPORARILY_UNAVAILABLE_TRY_TO_QUERY_ANOTHER_ADDRESS = "The server is temporarily unavailable. Try to query another address.";
    public static final String THERE_IS_NO_DATA_WITH_THIS_ID = "There is no data with this id";
    public static final String ADD_OR_UPDATE_FAILED = "addOrUpdate failed";
    public static final String DELETE_MORE_THEN_1_RECORD = "delete more then 1 record: ";
    public static final String CREATE_TABLE_FAILED = "create table failed";
    public static final String GET_PREPARED_STATEMENT_FAILED = "get preparedStatement failed";
    public static final String DID_T_CLOSE_RESULTSET = "did't close resultset";
    public static final String DID_T_CLOSE_PREPARED_STATEMENT = "did't close prepared statement";
    public static final String DID_T_CLOSE_CONNECTION = "did't close connection";
    public static final String CACHE_PROPERTIES = "cache.properties";
    public static final String CACHE = "cache";
    public static final String SIZE = "size";
    public static final String LOADFACTOR = "loadfactor";
    public static final String LFU = "lfu";
    public static final String LRU = "lru";
    public static final String ID = "id";
    public static final String JSON = "json";
    public static final String HAVE_T_KEY_IN_REQUEST = "Have't key in request!";
    public static final String METHOD = "_method";
    public static final String INITIAL_SESSION_FACTORY_CREATION_FAILED = "initial SessionFactory creation failed!";
    public static final String JDBC_DRIVER = "jdbc.driver";
    public static final String JDBC_URL = "jdbc.url";
    public static final String JDBC_USERNAME = "jdbc.username";
    public static final String JDBC_PASSWORD = "jdbc.password";
    public static final String HIBERNATE_DIALECT = "hibernate.dialect";
    public static final String HIBERNATE_HBM2DDL_AUTO = "hibernate.hbm2ddl.auto";
    public static final String HIBERNATE_CONNECTION_POOL_SIZE = "hibernate.connection.pool_size";
    public static final String HIBERNATE_CURRENT_SESSION_CONTEXT_CLASS = "hibernate.current_session_context_class";
    public static final String THREAD = "thread";
    public static final String HIBERNATE_SHOW_SQL = "hibernate.show_sql";
    public static final String BY_TRAINING = "by.training";
    public static final String CLASSPATH_APP_PROPERTIES = "classpath:app.properties";
    public static final String STATUS_PROPERTIES = "status.properties";
    public static final String INIT_CLUSTER_CONTEXT_IS_FAILED = "init ClusterContext is failed";
    public static final String SINGLE_THREAD_EXECUTOR_SERVICE_SHUTDOWN = "single thread executor service shutdown";
    public static final String ENVIRONMENT_VARIABLE_NOT_FOUND = "environment variable not found";
    public static final String LOAD_CLUSTER_STATUS_IS_FAILED = "load cluster status is failed";
    public static final String DON_T_SET_CLUSTER_STATUS = "don't set cluster status! please set!";
    public static final String TIMER_PROPERTIES = "timer.properties";
    public static final String INITIAL_DELAY = "initialDelay";
    public static final String PERIOD = "period";
    public static final String RUN_STATUS_CONTROLLER_FAILED = "run status controller failed";
    public static final String CURRENT_NODE_INDEX_NOT_FOUND = "current node index not found";
    public static final String GET_CURRENT_NODE_INDEX_FAILED = "get current node index failed";
    public static final String SEND = "send ";
    public static final String STATUS = "status";
    public static final String CLUSTER = "cluster";
    public static final String NOT_STATUS_PARAMETER = "not status parameter";
    public static final String NOT_STATUS_CHECK_ADDRESS = "not status: check address";
    public static final String REPLICATE = "replicate";
    public static final String REPLICA = "replica";
    public static final String SYNCHRO = "synchro";
    public static final String SYNCHRONIZE = "synchronize";
    public static final String FAILED = " failed";
    public static final String GET_RESPONSE_DATA = "get response data ";
    public static final String HTTP = "http ";
    public static final String EXECUTE_REQUEST = "execute request";
    public static final String CREATE_REQUEST = "create request";
    public static final String SET_ENCODING_TYPE = "set encoding type";
    public static final String ACLENTITY_ROW_CREATED = "ACLEntity row created";
}
