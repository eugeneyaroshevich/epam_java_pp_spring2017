package junit.validators;

import org.junit.Test;
import by.training.validator.HelloWorld;

import static org.junit.Assert.*;


public class HelloWorldTest {

    @Test
    public void testNewInstanseHelloWorldNotNullReturned(){
        HelloWorld helloWorld = new HelloWorld();

        assertNotNull(helloWorld);
    }

    @Test
    public void testMainRunWithArguments(){
        HelloWorld.main(new String[]{"Printable"});
    }

    @Test
    public void testMainRunWithOutArguments(){
        HelloWorld.main(new String[]{});
    }

}