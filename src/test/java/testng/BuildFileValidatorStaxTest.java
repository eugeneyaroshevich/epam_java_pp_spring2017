package testng;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import by.training.validator.BuildFileValidatorStax;

import javax.xml.stream.XMLStreamException;
import java.io.FileNotFoundException;

public class BuildFileValidatorStaxTest extends Assert {

    private BuildFileValidatorStax validator;

    @BeforeMethod
    public void before() {
        validator = new BuildFileValidatorStax();
    }

    @AfterMethod
    public void after() {
        validator = null;
    }

    // parseBuilds()
    @Test(groups = "parseBuilds", dependsOnMethods = "testCreateBuildFile")
    public void testParseBuildsNullReturned() throws FileNotFoundException, XMLStreamException {

        assertNull(validator.parseBuilds(null));
    }

    @Test(groups = "parseBuilds", dependsOnMethods = "testCreateBuildFile")
    public void testParseBuildsNotNullReturned() throws FileNotFoundException, XMLStreamException {

        assertNotNull(validator.parseBuilds(".\\build.xml"));
    }

    @Test(groups = "parseBuilds", expectedExceptions = FileNotFoundException.class)
    public void testParseBuildsThrowFileNotfoundException() throws FileNotFoundException, XMLStreamException {

        validator.parseBuilds(".");
    }

    @Test(groups = "parseBuilds", expectedExceptions = XMLStreamException.class)

    public void testParseBuildsThrowXMLStreamException() throws FileNotFoundException, XMLStreamException {

        validator.parseBuilds(".gitignore");
    }

    @Test(groups = "parseBuilds", timeOut = 300)

    public void testParseBuildsTimeOut() throws FileNotFoundException, XMLStreamException {

        assertNotNull(validator.parseBuilds(".\\build.xml"));
    }


    // createBuildFile()
    @Test
    public void testCreateBuildFile() {

        assertNotNull(validator.createBuildFile());
    }
}
