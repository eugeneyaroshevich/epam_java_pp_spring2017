package by.training.cache;

import java.util.LinkedHashMap;
import java.util.Map;

public class ConcurrentLRUCache<K, V> implements Cache<K, V> {

    private final LRUStorage storage;

    public ConcurrentLRUCache(int capacity, float loadFactor) {
        storage = new LRUStorage(capacity, loadFactor);
    }


    public static <K,V> ConcurrentLRUCache<K, V> newInstance(int size, float loadfactor) {
        return new ConcurrentLRUCache<>(size,loadfactor);
    }


    @Override
    public synchronized V get(K key) {
        return storage.get(key);
    }

    @Override
    public synchronized V put(K key, V value) {
        return storage.put(key, value);
    }

    @Override
    public V remove(K key) {
        V oldValue = storage.remove(key);
        if (oldValue == null) {
            return null;
        }
        return oldValue;

//        or if to use containsKey method

//        V oldValue = null;
//        if (storage.containsKey(key)){
//            oldValue = storage.remove(key);
//        }
//        return oldValue;
    }

    // not used
    @Override
    public boolean containsKey(K key) {
        return storage.containsKey(key);
    }

    @Override
    public int size() {
        return storage.size();
    }

    @Override
    public String toString() {
        return storage.toString();
    }


    private class LRUStorage extends LinkedHashMap<K, V> {

        private final int capacity;


        public LRUStorage(int initialCapacity, float loadFactor) {
            super(initialCapacity, loadFactor, true);
            this.capacity = initialCapacity;
        }

        @Override
        protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
            return size() > capacity;
        }

        @Override
        public String toString() {
            return super.toString();
        }
    }
}
