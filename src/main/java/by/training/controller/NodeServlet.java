package by.training.controller;

import by.training.context.ClusterContext;
import by.training.dao.ACLEntityDao;
import by.training.exception.ClusterException;
import by.training.network.ClusterHttpClient;
import by.training.service.JsonDataService;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;

import static by.training.constants.Constants.*;
import static by.training.network.ClusterHttpClient.*;
import static by.training.util.ClusterUtil.*;


public class NodeServlet extends HttpServlet implements Node {

    private static final Logger log = Logger.getLogger(NodeServlet.class);

    @Autowired
    protected ClusterContext context;
    @Autowired
    private ACLEntityDao aclEntityDao;
    @Autowired
    private JsonDataService jsonDataService;


    @Override
    public void init() throws ServletException {
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            setEncodingType(request, response);
            if (request.getParameter(SYNCHRO) != null) {
                super.service(request, response);
                return;
            }
            if (request.getParameter(STATUS) != null) {
                request.getRequestDispatcher(STATUS).forward(request, response);
                return;
            }
            if (!HttpOptions.METHOD_NAME.equals(getMethod(request)) && !context.isClusterStatus()) {
                throw new ClusterException(PLEASE_WAIT_CLUSTER_DON_T_READY_WORK);
            }
            if (HttpDelete.METHOD_NAME.equals(getMethod(request))) {
                doDelete(request, response);
                return;
            }
            super.service(request, response);
        } catch (Exception e) {
            log.error(this.getClass().getName() + getMethod(request) + FAILED, e);
            throw new ServletException(e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            Integer routeIndex = getRouteNodeIndex(getKey(request), context.getNodesSize());
            if (context.getCurrentNodeIndex() != routeIndex) {
                HttpResponse routeResponse = route(routeIndex, getMethod(request), context.getNodes(), context.getNodeStatuses(), getRequestParams(request));
                response.getWriter().write(getResponseData(routeResponse));
            } else {
                if (request.getParameter(REPLICATE) == null) {
                    request.getRequestDispatcher(REPLICA).forward(request, response);
                } else {
                    aclEntityDao.log(createACLEntity(request));
                    String json = jsonDataService.getById(getKey(request));
                    if (json != null) {
                        response.getWriter().write(json);
                    }
                }
            }
        } catch (Exception e) {
            log.error(this.getClass().getName() + getMethod(request) + FAILED, e);
            throw new ServletException(e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            Integer routeIndex = getRouteNodeIndex(getKey(request), context.getNodesSize());
            if (context.getCurrentNodeIndex() != routeIndex) {
                HttpResponse routeResponse = route(routeIndex, getMethod(request), context.getNodes(), context.getNodeStatuses(), getRequestParams(request));
                response.getWriter().write(getResponseData(routeResponse));
            } else {
                aclEntityDao.log(createACLEntity(request));
                String json = jsonDataService.add(getKey(request), getData(request));
                if (json != null) {
                    response.getWriter().write(json);
                }
                if (request.getParameter(REPLICATE) == null) {
                    request.getRequestDispatcher(REPLICA).forward(request, response);
                }
            }
        } catch (Exception e) {
            log.error(this.getClass().getName() + getMethod(request) + FAILED, e);
            throw new ServletException(e);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            Integer routeIndex = getRouteNodeIndex(getKey(request), context.getNodesSize());
            if (context.getCurrentNodeIndex() != routeIndex) {
                HttpResponse routeResponse = route(routeIndex, getMethod(request), context.getNodes(), context.getNodeStatuses(), getRequestParams(request));
                response.getWriter().write(getResponseData(routeResponse));
            } else {
                aclEntityDao.log(createACLEntity(request));
                String json = jsonDataService.delete(getKey(request));
                if (json != null) {
                    response.getWriter().write(json);
                }
                if (request.getParameter(REPLICATE) == null) {
                    request.getRequestDispatcher(REPLICA).forward(request, response);
                }
            }
        } catch (Exception e) {
            log.error(this.getClass().getName() + getMethod(request) + FAILED, e);
            throw new ServletException(e);
        }
    }

    @Override
    public HttpResponse route(int currentNodeIndex, String method, Properties nodes, Map<String, Boolean> nodeStatuses, Map<String, String> parameters) throws ClusterException {
        return ClusterHttpClient.route(currentNodeIndex, method, nodes, nodeStatuses, parameters);
    }
}
