package junit.reflection.a;

import org.junit.Test;
import by.training.reflection.a.PrinterProxy;
import by.training.reflection.a.ProxyFactory;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Proxy;
import java.util.Map;

import static org.junit.Assert.*;

public class ProxyFactoryTest {

    // new Instance

    @Test
    public void testProxyFactoryCreateNewInstanceNotNullReturned() {

        ProxyFactory proxyFactory = new ProxyFactory();

        assertNotNull(proxyFactory);
    }


    @Test
    public void testGetInstanceOfNotProxyClassFalseReturned() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {

        Class<?> clazz = ProxyFactory.getInstanceOf(Object.class).getClass();
        boolean isProxyClass = Proxy.isProxyClass(clazz);

        assertFalse(isProxyClass);
    }

    @Test
    public void testGetInstanceOfNotProxyClassTrueReturned() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {

        Class<?> clazz = ProxyFactory.getInstanceOf(PrinterProxy.class).getClass();
        boolean isProxyClass = Proxy.isProxyClass(clazz);

        assertTrue(isProxyClass);
    }

    @Test(expected = ClassNotFoundException.class)
    public void testThrowClassNotFoundException() throws ClassNotFoundException, NoSuchFieldException, IllegalAccessException, NoSuchMethodException, InvocationTargetException, InstantiationException {

        Class<?> printerClass = Class.forName("by.training.reflection.a.Printer");

        by.training.reflection.a.Proxy oldAnnotation = (by.training.reflection.a.Proxy) printerClass.getAnnotations()[0];

        Annotation newAnnotation = new by.training.reflection.a.Proxy() {
            @Override
            public Class<? extends Annotation> annotationType() {
                return oldAnnotation.annotationType();
            }

            @Override
            public String invocationHandler() {
                return "by.training.reflection.a.ProxyInvocationHand";
            }
        };

        Field annotationDataField = Class.class.getDeclaredField("annotationData");
        annotationDataField.setAccessible(true);
        Object annotationData = annotationDataField.get(printerClass);

        Field annotationsField = annotationData.getClass().getDeclaredField("annotations");
        annotationsField.setAccessible(true);

        Map<Class<? extends Annotation>, Annotation> annotations = (Map<Class<? extends Annotation>, Annotation>) annotationsField
                .get(annotationData);
        annotations.put(by.training.reflection.a.Proxy.class, newAnnotation);

        by.training.reflection.a.Proxy changedAnnottation = (by.training.reflection.a.Proxy) printerClass.getAnnotations()[0];
        System.out.println(changedAnnottation.invocationHandler()); // by.training.reflection.a.ProxyInvocationHand

        // before: by.training.reflection.a.ProxyInvocationHandler
        //after: by.training.reflection.a.ProxyInvocationHand

        ProxyFactory.getInstanceOf(printerClass);
    }
}