package by.training.reflection.a;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class ProxyInvocationHandler implements InvocationHandler {

    private Object obj;

    public ProxyInvocationHandler(Object obj) {
        this.obj = obj;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        Object result;

        try {

            System.out.println("I am Proxy!");
            System.out.println("before method " + method.getName());
            result = method.invoke(obj, args);

        } finally {
            System.out.println("after method " + method.getName());
        }

        return result;
    }
}
