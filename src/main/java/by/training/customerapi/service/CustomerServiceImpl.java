package by.training.customerapi.service;


import by.training.customerapi.entity.Customer;
import by.training.customerapi.dao.CustomerDao;
import by.training.exception.ClusterException;

public class CustomerServiceImpl implements CustomerService {


    private CustomerDao customerDao;

    public CustomerServiceImpl() {
    }

    public void setCustomerDao(CustomerDao customerDao) {
        this.customerDao = customerDao;
    }

    @Override
    public void add(Customer customer) throws ClusterException {
        customerDao.add(customer);
    }

    @Override
    public Customer getById(int id) throws ClusterException {
        return customerDao.getById(id);
    }

}
