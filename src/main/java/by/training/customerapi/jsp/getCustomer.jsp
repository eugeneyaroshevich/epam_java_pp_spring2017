<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="by.training.customerapi.entity.Customer" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Customer</title>
</head>
<body>
<%
    Customer customer = (Customer) request.getAttribute("customer");
%>
<table border="1">
    <tr>
        <th>Id</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Age</th>
    </tr>
    <tr>
        <td><%=customer.getId()%></td>
        <td><%=customer.getName()%></td>
        <td><%=customer.getSurname()%></td>
        <td><%=customer.getAge()%></td>
    </tr>
</table>
<p><a href="/by/training/customerapi/jsp/createCustomer.jsp">Add Customer</a></p>
<p><a href="/customer?action=showCustomer">Get Customer by ID</a></p>
</body>
</html>