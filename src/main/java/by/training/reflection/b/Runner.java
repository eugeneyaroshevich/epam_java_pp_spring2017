package by.training.reflection.b;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

public class Runner {

    public static void main(String[] args) {

        try {

            // runner of SerializationUtils.serialize / deserialize methods
            Bean serializeObj = new Bean("James", "Brown", 78, new Bean("InnerBean", "InnerBeanich", 55, null));

            Map<String, Object> serializemap = SerializationUtils.serialize(serializeObj);

            Bean deserializeObj = SerializationUtils.deserialize(serializemap, Bean.class);

            System.out.println(serializemap);
            System.out.println(deserializeObj);

            System.out.println(SerializationUtils.equalObjects(serializeObj, deserializeObj));

            serializeObj.setAge(3);
            System.out.println(SerializationUtils.equalObjects(serializeObj, deserializeObj));

            //        out:

            //        {surname=Brown, name=James, age=78, bean=name=InnerBean, surname=InnerBeanich, age=55, bean=null}
            //        name=James, surname=Brown, age=78, bean=name=InnerBean, surname=InnerBeanich, age=55, bean=null
            //        true
            //        false

        } catch (IllegalAccessException | InstantiationException | InvocationTargetException | IntrospectionException e) {
            e.printStackTrace();
        }
    }
}