package junit.caches;

import by.training.cache.Cache;
import by.training.cache.ConcurrentLRUCache;
import org.junit.*;

public class ConcurrentLRUCacheTest extends Assert {

    private Cache<Integer, Integer> concurrentLRUCache;

    @Before
    public void before() {
        concurrentLRUCache = new ConcurrentLRUCache<>(3, 0.75f);
    }


    // new Instance
    @Test
    public void testNewInstanceCacheNotNulReturned() {

        assertNotNull(concurrentLRUCache);
    }

    // test size() / put()
    @Test
    public void testSizeReturnSizeEqualsCapacity() {
        concurrentLRUCache.put(0, 0);
        concurrentLRUCache.put(1, 1);
        concurrentLRUCache.put(2, 2);
        concurrentLRUCache.put(3, 3);

        assertEquals(concurrentLRUCache.size(), 3);
    }

    @Test
    public void testSizeOverwritingValuesAfterPutEqualKeyAndDontChangeSizeAndRemainingElementsHaveValuesEqualOriginal() {

        concurrentLRUCache.put(0, 0);

        assertEquals(concurrentLRUCache.get(0).intValue(), 0);
        assertEquals(concurrentLRUCache.size(), 1);

        concurrentLRUCache.put(0, 1);

        assertEquals(concurrentLRUCache.size(), 1);
        assertNotEquals(concurrentLRUCache.get(0).intValue(), 0);
        assertEquals(concurrentLRUCache.get(0).intValue(), 1);
    }


    // test put()
    @Test
    public void testPutRemoveEldestEntryGetEldestKeyNullReturned() {

        concurrentLRUCache.put(0, 0);
        concurrentLRUCache.put(1, 1);
        concurrentLRUCache.put(2, 2);
        concurrentLRUCache.put(3, 3);

        assertNull(concurrentLRUCache.get(0));

    }

    @Test
    public void testPutRemoveEldestEntryGetYoungestKeyNotNullReturnedAndRemainingElementsHaveValuesEqualOriginal() {

        concurrentLRUCache.put(0, 0);
        concurrentLRUCache.put(1, 1);
        concurrentLRUCache.put(2, 2);
        concurrentLRUCache.put(3, 3);

        assertNull(concurrentLRUCache.get(0));

        assertNotNull(concurrentLRUCache.get(1));
        assertNotNull(concurrentLRUCache.get(2));
        assertNotNull(concurrentLRUCache.get(3));

        assertEquals(concurrentLRUCache.get(1).intValue(), 1);
        assertEquals(concurrentLRUCache.get(2).intValue(), 2);
        assertEquals(concurrentLRUCache.get(3).intValue(), 3);
    }

    @Test
    public void testPutRemoveEldestEntryGetYoungestValuesReturnedEqualsAndEldestElementsNullReturned() {

        concurrentLRUCache.put(1, 1);
        concurrentLRUCache.put(2, 2);
        concurrentLRUCache.put(3, 3);
        concurrentLRUCache.get(1);
        concurrentLRUCache.get(2);
        concurrentLRUCache.put(4, 4);
        concurrentLRUCache.put(5, 5);
        concurrentLRUCache.put(6, 6); // {4=4, 5=5, 6=6}

        assertNull(concurrentLRUCache.get(1));
        assertNull(concurrentLRUCache.get(2));
        assertNull(concurrentLRUCache.get(3));

        assertEquals(concurrentLRUCache.get(4).intValue(), 4);
        assertEquals(concurrentLRUCache.get(5).intValue(), 5);
        assertEquals(concurrentLRUCache.get(6).intValue(), 6);
    }

    @Test
    public void testPutInEmptyCacheNullReturned(){

        assertNull(concurrentLRUCache.put(0,0));
    }


    // test get()
    @Test
    public void testGetNotContainKeyNullReturned() {

        assertNull(concurrentLRUCache.get(0));
        assertNull(concurrentLRUCache.get(5));
        assertNull(concurrentLRUCache.get(7));
    }

    @Test
    public void testGetContainKeyNotNullReturneddAndElementsHaveValuesEqualOriginal() {

        concurrentLRUCache.put(0, 0);
        concurrentLRUCache.put(2, 2);

        assertNotNull(concurrentLRUCache.get(0));
        assertNotNull(concurrentLRUCache.get(2));

        assertEquals(concurrentLRUCache.get(0).intValue(), 0);
        assertEquals(concurrentLRUCache.get(2).intValue(), 2);
    }

    @Test
    public void testGetPutKeyValueReturnSameValueForThisKey() {

        Integer sameInteger = 0;
        concurrentLRUCache.put(0, sameInteger);

        assertSame(concurrentLRUCache.get(0), sameInteger);
    }

    // toString()
    @Test
    public void testToStringNotNullReturned() {

        assertNotNull(concurrentLRUCache.toString());
    }

    @Test
    public void testToStringCorrectStringReturned() {

        concurrentLRUCache.put(0,0);
        concurrentLRUCache.put(1,1);

        System.out.println(concurrentLRUCache.toString());

        assertEquals("{0=0, 1=1}",concurrentLRUCache.toString());
    }
}
