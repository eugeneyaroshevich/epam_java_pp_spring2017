package by.training.reflection.a;

@Proxy
public class PrinterProxy implements Printable {

    private Printable realPrinter;

    public Printable getRealPrinter() {
        return realPrinter;
    }

    public void setRealPrinter(Printable realPrinter) {
        this.realPrinter = realPrinter;
    }

    @Override
    public String print(String string) {

        if (realPrinter == null) {
            setRealPrinter(new Printer());
        }

        String realPrinterString = realPrinter.print(string);
        String proxyPrinterString = "Proxy";
        String resultString = realPrinterString + proxyPrinterString;
        System.out.println(resultString);

        return resultString;
    }
}
