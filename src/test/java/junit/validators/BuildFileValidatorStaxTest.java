package junit.validators;

import org.apache.tools.ant.BuildException;
import org.junit.*;
import by.training.validator.BuildFileValidatorStax;

import javax.xml.stream.XMLStreamException;
import java.io.FileNotFoundException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class BuildFileValidatorStaxTest extends Assert {

    private BuildFileValidatorStax validator;

    @Before
    public void before() {
        validator = new BuildFileValidatorStax();
    }

    @After
    public void after() {
        validator = null;
    }


    // is / setCheckdepends()
    @Test
    public void testIsCheckdependsSetTrueAndTrueReturned() {

        validator.setCheckdepends(true);

        assertTrue(validator.isCheckdepends());
    }

    @Test
    public void testIsCheckdependsSetFalseAndTrueReturned() {

        validator.setCheckdepends(false);

        assertFalse(validator.isCheckdepends());
    }

    @Test
    public void testSetCheckdependsSetTrueAndTrueReturned() {

        validator.setCheckdepends(true);

        assertTrue(validator.isCheckdepends());
    }

    @Test
    public void testSetCheckdependsSetFalseAndTrueReturned() {

        validator.setCheckdepends(false);

        assertFalse(validator.isCheckdepends());
    }


    // is / setCheckdefault()
    @Test
    public void testIsCheckdefaultSetTrueAndTrueReturned() {

        validator.setCheckdefault(true);

        assertTrue(validator.isCheckdefault());
    }

    @Test
    public void testIsCheckdefaultSetFalseAndTrueReturned() {

        validator.setCheckdefault(false);

        assertFalse(validator.isCheckdefault());
    }

    @Test
    public void testSetCheckdefaultSetTrueAndTrueReturned() {

        validator.setCheckdefault(true);

        assertTrue(validator.isCheckdefault());
    }

    @Test
    public void testSetCheckdefaultSetFalseAndTrueReturned() {

        validator.setCheckdefault(false);

        assertFalse(validator.isCheckdefault());
    }


    // is / setChecknames()
    @Test
    public void testIsChecknamesSetTrueAndTrueReturned() {

        validator.setChecknames(true);

        assertTrue(validator.isChecknames());
    }

    @Test
    public void testIsChecknamesSetFalseAndTrueReturned() {

        validator.setChecknames(false);

        assertFalse(validator.isChecknames());
    }

    @Test
    public void testSetChecknamesSetTrueAndTrueReturned() {

        validator.setChecknames(true);

        assertTrue(validator.isChecknames());
    }

    @Test
    public void testSetChecknamesSetFalseAndTrueReturned() {

        validator.setChecknames(false);

        assertFalse(validator.isChecknames());
    }


    // BuildFile class
    @Test
    public void testCreateNewInstanceBuildFileNotNullReturned() {

        BuildFileValidatorStax.Buildfile buildfile = validator.new Buildfile();

        assertNotNull(buildfile);
    }

    @Test
    public void testBuildFileSetGetLocationEqualsLocationsName() {
        BuildFileValidatorStax.Buildfile buildfile = validator.new Buildfile();
        buildfile.setLocation("build.xml");

        assertEquals("build.xml", buildfile.getLocation());
    }

    @Test
    public void testBuildFileSetGetLocationNotNullReturned() {
        BuildFileValidatorStax.Buildfile buildfile = validator.new Buildfile();
        buildfile.setLocation("build.xml");

        assertNotNull(buildfile.getLocation());
    }


    // createBuildFile()
    @Test
    public void testCreateBuildFile() {

        assertNotNull(validator.createBuildFile());
    }


    // getBuildsFiles()
    @Test
    public void testGetBuildFilesNotNullReturned() {

        assertNotNull(validator.getBuildfiles());
    }

    @Test
    public void testGetBuildFilesAllBuildFilesNotNullReturned() {

        for (BuildFileValidatorStax.Buildfile buildfile : validator.getBuildfiles()) {
            assertNotNull(buildfile);
        }
    }

    @Test
    public void testGetBuildFilesAllBuildFilesGetLocationNotNullReturned() {

        for (BuildFileValidatorStax.Buildfile buildfile : validator.getBuildfiles()) {
            assertNotNull(buildfile.getLocation());
        }
    }

    @Test(expected = NullPointerException.class)
    public void testSetBuildFilesThowNullPointerException() {

        BuildFileValidatorStax.Buildfile buildfile = validator.createBuildFile();
        buildfile.setLocation(null);

    }


    // parseBuilds()
    @Test
    public void testParseBuildsNullReturned() throws FileNotFoundException, XMLStreamException {

        assertNull(validator.parseBuilds(null));
    }

    @Test
    public void testParseBuildsNotNullReturned() throws FileNotFoundException, XMLStreamException {

        assertNotNull(validator.parseBuilds(".\\build.xml"));
    }

    @Test(expected = FileNotFoundException.class)
    public void testParseBuildsThrowFileNotfoundException() throws FileNotFoundException, XMLStreamException {

        validator.parseBuilds(".");
    }

    @Test(expected = XMLStreamException.class)
    public void testParseBuildsThrowXMLStreamException() throws FileNotFoundException, XMLStreamException {

        validator.parseBuilds(".gitignore");
    }

    @Test(timeout = 300)
    public void testParseBuildsTimeOut() throws FileNotFoundException, XMLStreamException {

        assertNotNull(validator.parseBuilds(".\\build.xml"));
    }


    // execute()
    @Test
    public void testExecuteThrowBuildExceptionEqualClassName() throws NoSuchFieldException, IllegalAccessException {
        BuildFileValidatorStax.Buildfile buildfile = validator.createBuildFile();
        buildfile.setLocation(".gitignore");
        Collection<BuildFileValidatorStax.Buildfile> buildfiles = new ArrayList<>();
        buildfiles.add(buildfile);

        Field buildfilesField = validator.getClass().getDeclaredField("buildfiles");
        buildfilesField.setAccessible(true);
        buildfilesField.set(validator, buildfiles);
        buildfilesField.setAccessible(false);

        try {
            validator.execute();
        }catch (Exception e){
            assertEquals("class org.apache.tools.ant.BuildException",e.getClass().toString());
        }
    }

    @Test(expected = BuildException.class)
    public void testExecuteThrowBuildException() throws NoSuchFieldException, IllegalAccessException {
        BuildFileValidatorStax.Buildfile buildfile = validator.createBuildFile();
        buildfile.setLocation("buil.xml");
        Collection<BuildFileValidatorStax.Buildfile> buildfiles = new ArrayList<>();
        buildfiles.add(buildfile);

        Field buildfilesField = validator.getClass().getDeclaredField("buildfiles");
        buildfilesField.setAccessible(true);
        buildfilesField.set(validator, buildfiles);
        buildfilesField.setAccessible(false);

        validator.execute();
    }

    @Test(timeout = 300)
    public void testExecuteTimeOut() {
        validator.execute();
    }


    // Property class
    @Test
    public void testPropertyClassToStringNotNullReturned() throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException {

        assertNotNull(createObject(BuildFileValidatorStax.class, "Property").toString());
    }

    @Test
    public void testPropertyClassGetName() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {

        Object property = createObject(BuildFileValidatorStax.class, "Property");

        Method method = property.getClass().getDeclaredMethod("getName");
        method.setAccessible(true);
        String s = (String) method.invoke(property);
        method.setAccessible(false);

        assertEquals(s, "name");
    }

    @Test
    public void testPropertyClassGetLocation() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {

        Object property = createObject(BuildFileValidatorStax.class, "Property");

        Method method = property.getClass().getDeclaredMethod("getLocation");
        method.setAccessible(true);
        String s = (String) method.invoke(property);
        method.setAccessible(false);

        assertEquals(s, "location");
    }


    //Sequential
    @Test
    public void testSequentialClassToStringNotNullReturned() throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException {

        assertNotNull(createObject(BuildFileValidatorStax.class, "Sequential").toString());
    }


    //CheckBean
    @Test
    public void testCheckBeanClassToStringNotNullReturned() throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchFieldException {

        Object checkbean = createObject(BuildFileValidatorStax.class, "CheckBean");
        Object property = createObject(BuildFileValidatorStax.class, "Property");
        Object sequential = createObject(BuildFileValidatorStax.class, "Sequential");

        Field sequentialField = checkbean.getClass().getDeclaredField("sequential");
        sequentialField.setAccessible(true);
        sequentialField.set(checkbean, sequential);
        sequentialField.setAccessible(false);

        Field propertiesField = checkbean.getClass().getDeclaredField("properties");
        propertiesField.setAccessible(true);
        ArrayList arrayList = new ArrayList();
        arrayList.add(property);
        propertiesField.set(checkbean, arrayList);
        sequentialField.setAccessible(false);

        assertNotNull(checkbean.toString());
    }

    @Test
    public void testCheckBeanClassCheckDependsTrueReturned() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, NoSuchFieldException {
        Object checkbean = createObject(BuildFileValidatorStax.class, "CheckBean");
        Object sequential = createObject(BuildFileValidatorStax.class, "Sequential");

        Field dependsList = sequential.getClass().getDeclaredField("depends");
        dependsList.setAccessible(true);
        List<String> list = new ArrayList<>();
        list.add("javac");
        list.add("jar");
        list.add("java");
        dependsList.set(sequential, list);
        dependsList.setAccessible(false);

        Field sequentialField = checkbean.getClass().getDeclaredField("sequential");
        sequentialField.setAccessible(true);
        sequentialField.set(checkbean, sequential);
        sequentialField.setAccessible(false);

        Method method = checkbean.getClass().getDeclaredMethod("checkdepends");
        method.setAccessible(true);

        boolean check = (boolean) method.invoke(checkbean);
        method.setAccessible(false);

        assertTrue(check);
    }

    @Test
    public void testCheckBeanClassCheckDependsFalseReturned() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, NoSuchFieldException {
        Object checkbean = createObject(BuildFileValidatorStax.class, "CheckBean");
        Object sequential = createObject(BuildFileValidatorStax.class, "Sequential");

        Field dependsList = sequential.getClass().getDeclaredField("depends");
        dependsList.setAccessible(true);
        List<String> list = new ArrayList<>();
        list.add("java");
        list.add("jar");
        list.add("java");
        dependsList.set(sequential, list);
        dependsList.setAccessible(false);

        Field sequentialField = checkbean.getClass().getDeclaredField("sequential");
        sequentialField.setAccessible(true);
        sequentialField.set(checkbean, sequential);
        sequentialField.setAccessible(false);

        Method method = checkbean.getClass().getDeclaredMethod("checkdepends");
        method.setAccessible(true);

        boolean check = (boolean) method.invoke(checkbean);
        method.setAccessible(false);

        assertFalse(check);
    }

    @Test
    public void testCheckBeanClassCheckDefaultTrueReturned() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, NoSuchFieldException {

        Object checkbean = createObject(BuildFileValidatorStax.class, "CheckBean");
        Object property = createObject(BuildFileValidatorStax.class, "Property");

        Field propertiesField = checkbean.getClass().getDeclaredField("properties");
        propertiesField.setAccessible(true);
        ArrayList arrayList = new ArrayList();
        arrayList.add(property);
        propertiesField.set(checkbean, arrayList);
        propertiesField.setAccessible(false);

        Method method = checkbean.getClass().getDeclaredMethod("checkdefault");
        method.setAccessible(true);

        boolean check = (boolean) method.invoke(checkbean);
        method.setAccessible(false);

        assertTrue(check);
    }

    @Test
    public void testCheckBeanClassCheckDefaultNameFieldNullAndFalseReturned() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, NoSuchFieldException {

        Object checkbean = createObject(BuildFileValidatorStax.class, "CheckBean");
        Object property = createObject(BuildFileValidatorStax.class, "Property");

        Field name = property.getClass().getDeclaredField("name");
        name.setAccessible(true);
        name.set(property, null);
        name.setAccessible(false);

        Field propertiesField = checkbean.getClass().getDeclaredField("properties");
        propertiesField.setAccessible(true);
        ArrayList arrayList = new ArrayList();
        arrayList.add(property);
        propertiesField.set(checkbean, arrayList);
        propertiesField.setAccessible(false);

        Method method = checkbean.getClass().getDeclaredMethod("checkdefault");
        method.setAccessible(true);

        boolean check = (boolean) method.invoke(checkbean);
        method.setAccessible(false);

        assertFalse(check);
    }

    @Test
    public void testCheckBeanClassCheckDefaultLocationFieldNullAndFalseReturned() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, NoSuchFieldException {

        Object checkbean = createObject(BuildFileValidatorStax.class, "CheckBean");
        Object property = createObject(BuildFileValidatorStax.class, "Property");

        Field name = property.getClass().getDeclaredField("location");
        name.setAccessible(true);
        name.set(property, null);
        name.setAccessible(false);

        Field propertiesField = checkbean.getClass().getDeclaredField("properties");
        propertiesField.setAccessible(true);
        ArrayList arrayList = new ArrayList();
        arrayList.add(property);
        propertiesField.set(checkbean, arrayList);
        propertiesField.setAccessible(false);

        Method method = checkbean.getClass().getDeclaredMethod("checkdefault");
        method.setAccessible(true);

        boolean check = (boolean) method.invoke(checkbean);
        method.setAccessible(false);

        assertFalse(check);
    }


    @Test
    public void testCheckBeanClassCheckNamesLocationFieldNullAndFalseReturned() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, NoSuchFieldException {

        Object checkbean = createObject(BuildFileValidatorStax.class, "CheckBean");
        Object property = createObject(BuildFileValidatorStax.class, "Property");

        Field name = property.getClass().getDeclaredField("location");
        name.setAccessible(true);
        name.set(property, null);
        name.setAccessible(false);

        Field propertiesField = checkbean.getClass().getDeclaredField("properties");
        propertiesField.setAccessible(true);
        ArrayList arrayList = new ArrayList();
        arrayList.add(property);
        propertiesField.set(checkbean, arrayList);
        propertiesField.setAccessible(false);

        Method method = checkbean.getClass().getDeclaredMethod("checknames");
        method.setAccessible(true);

        boolean check = (boolean) method.invoke(checkbean);
        method.setAccessible(false);

        assertFalse(check);
    }

    @Test
    public void testCheckBeanClassCheckNamesLocationFieldNotPatternMatchesAndFalseReturned() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, NoSuchFieldException {

        Object checkbean = createObject(BuildFileValidatorStax.class, "CheckBean");
        Object property = createObject(BuildFileValidatorStax.class, "Property");

        Field name = property.getClass().getDeclaredField("location");
        name.setAccessible(true);
        name.set(property, "$$$");
        name.setAccessible(false);

        Field propertiesField = checkbean.getClass().getDeclaredField("properties");
        propertiesField.setAccessible(true);
        ArrayList arrayList = new ArrayList();
        arrayList.add(property);
        propertiesField.set(checkbean, arrayList);
        propertiesField.setAccessible(false);

        Method method = checkbean.getClass().getDeclaredMethod("checknames");
        method.setAccessible(true);

        boolean check = (boolean) method.invoke(checkbean);
        method.setAccessible(false);

        assertFalse(check);
    }

    @Test
    public void testCheckBeanClassCheckNamesTrueReturned() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, NoSuchFieldException {

        Object checkbean = createObject(BuildFileValidatorStax.class, "CheckBean");
        Object property = createObject(BuildFileValidatorStax.class, "Property");

        Field propertiesField = checkbean.getClass().getDeclaredField("properties");
        propertiesField.setAccessible(true);
        ArrayList arrayList = new ArrayList();
        arrayList.add(property);
        propertiesField.set(checkbean, arrayList);
        propertiesField.setAccessible(false);

        Method method = checkbean.getClass().getDeclaredMethod("checknames");
        method.setAccessible(true);

        boolean check = (boolean) method.invoke(checkbean);
        method.setAccessible(false);

        assertTrue(check);
    }


    @Test
    public void testCheckBeanClassCheckAllEqualsFailedMessagesTrueReturned() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {

        Object checkbean = createObject(BuildFileValidatorStax.class, "CheckBean");

        Method method = checkbean.getClass().getDeclaredMethod("checkAll", boolean.class, boolean.class, boolean.class);
        method.setAccessible(true);

        ArrayList<String> check = (ArrayList<String>) method.invoke(checkbean, false, false, false);
        method.setAccessible(false);

        assertEquals(check.size(), 3);
        assertEquals(check.get(0), "Check depends verification was not performed");
        assertEquals(check.get(1), "Check default verification was not performed");
        assertEquals(check.get(2), "Check names verification was not performed");
    }

    @Test
    public void testCheckBeanClassCheckAllEqualsPositiveMessagesTrueReturned() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, NoSuchFieldException {

        Object checkbean = createObject(BuildFileValidatorStax.class, "CheckBean");
        Object property = createObject(BuildFileValidatorStax.class, "Property");
        Object sequential = createObject(BuildFileValidatorStax.class, "Sequential");

        Field dependsList = sequential.getClass().getDeclaredField("depends");
        dependsList.setAccessible(true);
        List<String> list = new ArrayList<>();
        list.add("javac");
        list.add("jar");
        list.add("java");
        dependsList.set(sequential, list);
        dependsList.setAccessible(false);

        Field sequentialField = checkbean.getClass().getDeclaredField("sequential");
        sequentialField.setAccessible(true);
        sequentialField.set(checkbean, sequential);
        sequentialField.setAccessible(false);

        Field propertiesField = checkbean.getClass().getDeclaredField("properties");
        propertiesField.setAccessible(true);
        ArrayList arrayList = new ArrayList();
        arrayList.add(property);
        propertiesField.set(checkbean, arrayList);
        sequentialField.setAccessible(false);


        Method method = checkbean.getClass().getDeclaredMethod("checkAll", boolean.class, boolean.class, boolean.class);
        method.setAccessible(true);

        ArrayList<String> check = (ArrayList<String>) method.invoke(checkbean, true, true, true);
        method.setAccessible(false);

        assertEquals(check.size(), 3);
        assertEquals(check.get(0), "true");
        assertEquals(check.get(1), "true");
        assertEquals(check.get(2), "true");
    }

    private Object createObject(Class<?> clazz, String simpleNameClass) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException {

        Object o = null;

        Class<?>[] classes = clazz.getDeclaredClasses();
        for (Class<?> c : classes) {
            if (simpleNameClass.equals(c.getSimpleName())) {
                Class<?> aClass = Class.forName("by.training.validator.BuildFileValidatorStax$" + simpleNameClass);
                Constructor<?> constructor;
                if ("Property".equals(simpleNameClass)) {
                    constructor = aClass.getDeclaredConstructor(BuildFileValidatorStax.class, String.class, String.class);
                    constructor.setAccessible(true);
                    o = constructor.newInstance(BuildFileValidatorStax.class.newInstance(), "name", "location");
                } else {
                    constructor = aClass.getDeclaredConstructor(BuildFileValidatorStax.class);
                    constructor.setAccessible(true);
                    o = constructor.newInstance(BuildFileValidatorStax.class.newInstance());
                }
            }
        }
        return o;
    }
}
