//package by.training.customerapi.test.dao;
//
//import by.training.customerapi.dao.CustomerDao;
//import by.training.customerapi.dao.CustomerDaoImpl;
//import by.training.customerapi.dao.DaoFactory;
//import org.junit.Test;
//
//import static org.junit.Assert.*;
//
//public class DaoFactoryTest {
//
//    @Test
//    public void testNewInstanceDaoFactory(){
//        DaoFactory daoFactory = new DaoFactory();
//
//        Assert.assertNotNull(daoFactory);
//
//    }
//
//    @Test
//    public void testGetCustomerDao(){
//
//        CustomerDao customerDao = DaoFactory.getCustomerDaoImpl();
//
//        Assert.assertNotNull(customerDao);
//        Assert.assertTrue(customerDao instanceof CustomerDaoImpl);
//    }
//}