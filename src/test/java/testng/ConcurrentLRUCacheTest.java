package testng;

import by.training.cache.Cache;
import by.training.cache.ConcurrentLRUCache;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;


public class ConcurrentLRUCacheTest extends Assert {

    private Cache<Integer, Integer> concurrentLRUCache;

    @BeforeMethod
    public void before() {
        concurrentLRUCache = new ConcurrentLRUCache<>(3, 0.75f);
    }

    @AfterMethod
    public void after() {
        concurrentLRUCache = null;
    }


    // test size() / put()
    @Test(groups = "size")
    public void testSizeReturnSizeEqualsCapacity() {
        concurrentLRUCache.put(0, 0);
        concurrentLRUCache.put(1, 1);
        concurrentLRUCache.put(2, 2);
        concurrentLRUCache.put(3, 3);

        assertEquals(concurrentLRUCache.size(), 3);
    }

    @Test(groups = "size")
    public void testSizeOwerwritingValuesWithTheSameKeysTwoReturned() {

        concurrentLRUCache.put(0, 0);
        concurrentLRUCache.put(1, 1);
        concurrentLRUCache.put(1, 2);

        assertEquals(concurrentLRUCache.size(), 2);
    }


    // test put()
    @Test(groups = "put", dependsOnMethods = "testSizeReturnSizeEqualsCapacity")
    public void testPutRemoveEldestEntryGetEldestKeyNullReturned() {
        concurrentLRUCache.put(0, 0);
        concurrentLRUCache.put(1, 1);
        concurrentLRUCache.put(2, 2);
        concurrentLRUCache.put(3, 3);

        assertNull(concurrentLRUCache.get(0));
    }

    @Test(groups = "put", dependsOnMethods = "testSizeOwerwritingValuesWithTheSameKeysTwoReturned")
    public void testPutRemoveEldestEntryGetYoungestKeyNotNullReturned() {
        concurrentLRUCache.put(0, 0);
        concurrentLRUCache.put(1, 1);
        concurrentLRUCache.put(2, 2);
        concurrentLRUCache.put(3, 3);

        assertNotNull(concurrentLRUCache.get(1));
        assertNotNull(concurrentLRUCache.get(2));
        assertNotNull(concurrentLRUCache.get(3));
    }

    @Test(groups = "put")
    public void testPutRemoveEldestEntryGetYoungestValuesReturnedEquals() {
        concurrentLRUCache.put(1, 1);
        concurrentLRUCache.put(2, 2);
        concurrentLRUCache.put(3, 3);
        concurrentLRUCache.get(1);
        concurrentLRUCache.put(4, 4);
        concurrentLRUCache.put(5, 5);
        concurrentLRUCache.get(2);
        concurrentLRUCache.put(6, 6); // {4=4, 5=5, 6=6}

        assertEquals(concurrentLRUCache.get(4), new Integer(4));
        assertEquals(concurrentLRUCache.get(5), new Integer(5));
        assertEquals(concurrentLRUCache.get(6), new Integer(6));
    }

    // test get()
    @Test(groups = "get")
    public void testGetNotContainKeyNullReturned() {

        assertEquals(concurrentLRUCache.size(), 0);

        assertNull(concurrentLRUCache.get(0));
        assertNull(concurrentLRUCache.get(5));
        assertNull(concurrentLRUCache.get(7));
    }

    @Test(groups = "get")
    public void testGetContainKeyNotNullReturned() {

        concurrentLRUCache.put(0, 0);
        concurrentLRUCache.put(2, 2);

        assertNotNull(concurrentLRUCache.get(0));
        assertNotNull(concurrentLRUCache.get(2));
    }

    @Test(groups = "get")
    public void testGetPutKeyValueReturnThisValueForThisKey() {

        concurrentLRUCache.put(0, 0);
        concurrentLRUCache.put(7, 7);

        assertEquals(concurrentLRUCache.get(0), new Integer(0));
        assertEquals(concurrentLRUCache.get(7), new Integer(7));

    }

    @Test(groups = "get")
    public void testGetPutKeyValueReturnSameValueForThisKey() {

        Integer sameInteger = 0;
        concurrentLRUCache.put(0, sameInteger);

        assertSame(concurrentLRUCache.get(0), sameInteger);
    }

    @Test(groups = "get")
    public void testGetPutKeyValueReturnNotSameValueForThisKey() {

        Integer sameInteger = 0;
        concurrentLRUCache.put(0, sameInteger);

        Integer notSameInteger = 1;

        assertNotSame(concurrentLRUCache.get(0), notSameInteger);
    }

}
