package by.training.customerapi.dao;

import by.training.customerapi.entity.Customer;
import by.training.exception.ClusterException;

public interface CustomerDao {

    void add(Customer customer) throws ClusterException;

    Customer getById(int id) throws ClusterException;

}
