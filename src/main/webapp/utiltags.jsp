<%@ taglib prefix="utils" uri="utils" %>

<html>
<body>

<utils:listmapping>
    ${url} - ${servlet}
</utils:listmapping>

<utils:resolveurl url="/error.jsp">
    ${url} - ${resource}
</utils:resolveurl>

</body>
</html>

