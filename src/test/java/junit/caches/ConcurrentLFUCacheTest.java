package junit.caches;

import by.training.cache.Cache;
import by.training.cache.ConcurrentLFUCache;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ConcurrentLFUCacheTest extends Assert {

    private Cache<Integer, Integer> concurrentLFUCache;

    @Before
    public void before() {
        concurrentLFUCache = new ConcurrentLFUCache<>();
    }


    //newInstance()
    @Test
    public void testCreateNewInstanceNotNullReturned() {
        assertNotNull(ConcurrentLFUCache.newInstance());
    }

    @Test
    public void testCreateNewInstanceWithCapacityConstructorNotNullReturned() {
        assertNotNull(ConcurrentLFUCache.newInstance(7));
    }

    // size() / put()
    @Test
    public void testSizeReturnSizeEqualsCapacity() {
        concurrentLFUCache.put(0, 0);
        concurrentLFUCache.put(1, 1);
        concurrentLFUCache.put(2, 2);
        concurrentLFUCache.put(3, 3);

        assertEquals(concurrentLFUCache.size(), 3);
    }

    @Test
    public void testSizeOverwritingValuesAfterPutEqualKeyAndDontChangeSizeAndRemainingElementsHaveValuesEqualOriginal() {

        concurrentLFUCache.put(0, 0);

        assertEquals(concurrentLFUCache.get(0).intValue(), 0);
        assertEquals(concurrentLFUCache.size(), 1);

        concurrentLFUCache.put(0, 1);

        assertEquals(concurrentLFUCache.size(), 1);
        assertNotEquals(concurrentLFUCache.get(0).intValue(), 0);
        assertEquals(concurrentLFUCache.get(0).intValue(), 1);
    }


    // test put()
    @Test
    public void testPutIfSizeOfCacheEqualCapacityRemoveEldestEntryGetEldestKeyNullReturned() {
        concurrentLFUCache.put(0, 0);
        concurrentLFUCache.put(1, 1);
        concurrentLFUCache.put(2, 2);
        concurrentLFUCache.put(3, 3);

        assertNull(concurrentLFUCache.get(0));
    }

    @Test
    public void testPutRemoveEldestEntryGetYoungestKeyNotNullReturnedAndRemainingElementsHaveValuesEqualOriginal() {

        concurrentLFUCache.put(0, 0);
        concurrentLFUCache.put(1, 1);
        concurrentLFUCache.put(2, 2);
        concurrentLFUCache.put(3, 3);

        assertNotNull(concurrentLFUCache.get(1));
        assertNotNull(concurrentLFUCache.get(2));
        assertNotNull(concurrentLFUCache.get(3));

        assertEquals(concurrentLFUCache.get(1).intValue(), 1);
        assertEquals(concurrentLFUCache.get(2).intValue(), 2);
        assertEquals(concurrentLFUCache.get(3).intValue(), 3);
    }

    @Test
    public void testPutRemoveEldestEntryGetYoungestValuesReturnedEquals() {

        concurrentLFUCache.put(0, 0);
        concurrentLFUCache.put(1, 1);
        concurrentLFUCache.get(1);
        concurrentLFUCache.put(2, 2);
        concurrentLFUCache.get(2);
        concurrentLFUCache.put(3, 3);
        concurrentLFUCache.put(4, 4);
        concurrentLFUCache.get(3);
        concurrentLFUCache.put(5, 5);
        concurrentLFUCache.put(6, 6);
        concurrentLFUCache.get(6);
        concurrentLFUCache.put(7, 7); // {key=2, value=2, key=6, value=6, key=7, value=7}

        assertEquals(concurrentLFUCache.get(2).intValue(), 2);
        assertEquals(concurrentLFUCache.get(6).intValue(), 6);
        assertEquals(concurrentLFUCache.get(7).intValue(), 7);
    }

    @Test
    public void testPutInEmptyCacheNullReturned(){

        assertNull(concurrentLFUCache.put(0,0));
    }

    @Test
    public void testPutIfCacheContainElementsWithEqualFrequencyDeletionOnSystemTimeAndRemainingElementsHaveValuesEqualOriginal(){

        concurrentLFUCache.put(0,0);
        concurrentLFUCache.put(1,1);
        concurrentLFUCache.put(2,2);

        concurrentLFUCache.get(2);
        concurrentLFUCache.get(0);
        concurrentLFUCache.get(1);

        concurrentLFUCache.put(3,3);

        assertNull(concurrentLFUCache.get(2));

        assertEquals(concurrentLFUCache.get(0).intValue(),0);
        assertEquals(concurrentLFUCache.get(1).intValue(),1);
        assertEquals(concurrentLFUCache.get(3).intValue(),3);
    }


    // get()
    @Test
    public void testGetNotContainKeyNullReturned() {

        assertNull(concurrentLFUCache.get(0));
        assertNull(concurrentLFUCache.get(5));
        assertNull(concurrentLFUCache.get(7));
    }

    @Test
    public void testGetContainKeyNotNullReturneddAndElementsHaveValuesEqualOriginal() {

        concurrentLFUCache.put(0, 0);
        concurrentLFUCache.put(2, 2);

        assertNotNull(concurrentLFUCache.get(0));
        assertNotNull(concurrentLFUCache.get(2));

        assertEquals(concurrentLFUCache.get(0).intValue(), 0);
        assertEquals(concurrentLFUCache.get(2).intValue(), 2);
    }

    @Test
    public void testGetPutKeyValueReturnValuesEqualOriginal() {

        concurrentLFUCache.put(0, 0);
        concurrentLFUCache.put(7, 7);

        assertEquals(concurrentLFUCache.get(0).intValue(), 0);
        assertEquals(concurrentLFUCache.get(7).intValue(), 7);

    }

    @Test
    public void testGetPutKeyValueReturnSameValueForThisKey() {

        Integer sameInteger = 0;
        concurrentLFUCache.put(0, sameInteger);

        assertSame(concurrentLFUCache.get(0), sameInteger);
    }

    // toString()
    @Test
    public void testToStringNotNullReturned() {

        assertNotNull(concurrentLFUCache.toString());
    }

    @Test
    public void testToStringCorrectStringReturned() {

        concurrentLFUCache.put(0,0);

        System.out.println(concurrentLFUCache.toString());

        assertEquals("{0= is key, 0 is value}",concurrentLFUCache.toString());
    }
}
