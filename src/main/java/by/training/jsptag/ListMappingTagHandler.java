package by.training.jsptag;

import javax.servlet.ServletRegistration;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.BodyTagSupport;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

public class ListMappingTagHandler extends BodyTagSupport {

    private Iterator<? extends Map.Entry<String, ? extends ServletRegistration>> iterator;


    public int doStartTag() throws JspException {
        try {
            pageContext.getOut().write("<p>");
            Map<String, ? extends ServletRegistration> map = pageContext.getServletContext().getServletRegistrations();
            iterator = map.entrySet().iterator();
            setAttribute(iterator.next());
            return EVAL_BODY_INCLUDE;
        } catch (IOException e) {
            throw new JspTagException(e.getMessage());
        }
    }

    public int doAfterBody() throws JspException {
        try {
            if (iterator.hasNext()) {
                setAttribute(iterator.next());
                pageContext.getOut().write("</p>");
                return EVAL_BODY_AGAIN;
            }
            return SKIP_BODY;
        } catch (IOException e) {
            throw new JspTagException(e.getMessage());
        }
    }

    public int doEndTag() throws JspException {
        try {
            pageContext.getOut().write("<br>-----------------<br>");
        } catch (IOException e) {
            throw new JspTagException(e.getMessage());
        }

        return EVAL_PAGE;
    }

    private void setAttribute(Map.Entry<String, ? extends ServletRegistration> entry) {
        String key = entry.getKey();
        ServletRegistration registration = pageContext.getServletContext().getServletRegistration(key);
        Collection<String> mappings = registration.getMappings();
        pageContext.setAttribute("url", mappings);
        pageContext.setAttribute("servlet", key);
    }
}
