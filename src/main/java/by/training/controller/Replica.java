package by.training.controller;


import by.training.exception.ClusterException;
import org.apache.http.HttpResponse;

import java.util.Map;
import java.util.Properties;

public interface Replica {

    HttpResponse replicate(int currentNodeIndex, String currentNodeUrl, String method, Properties nodes, Map<String, Boolean> nodeStatuses, Map<String, String> parameters) throws ClusterException;
}
