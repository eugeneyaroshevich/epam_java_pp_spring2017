package by.training.controller;

import by.training.context.ClusterContext;
import by.training.dao.ACLEntityDao;
import by.training.exception.ClusterException;
import by.training.network.ClusterHttpClient;
import by.training.service.JsonDataService;
import org.apache.http.HttpResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;

import static by.training.constants.Constants.*;
import static by.training.network.ClusterHttpClient.*;
import static by.training.util.ClusterUtil.*;


public class ReplicaServlet extends HttpServlet implements Replica {

    private static final Logger log = Logger.getLogger(ReplicaServlet.class);


    @Autowired
    protected ClusterContext context;
    @Autowired
    private ACLEntityDao aclEntityDao;
    @Autowired
    private JsonDataService jsonDataService;


    @Override
    public void init() throws ServletException {
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            Map<String, String> parameters = getRequestParams(request);
            parameters.put(REPLICATE, REPLICATE);
            HttpResponse replicateResponse = replicate(context.getCurrentNodeIndex(), context.getCurrentNodeUrl(),
                    getMethod(request), context.getNodes(), context.getNodeStatuses(), parameters);
            if (replicateResponse != null) {
                response.getWriter().write(getResponseData(replicateResponse));
            } else {
                aclEntityDao.log(createACLEntity(request));
                String json = jsonDataService.getById(getKey(request));
                if (json != null) {
                    response.getWriter().write(json);
                }
            }
        } catch (Exception e) {
            log.error(this.getClass().getName() + getMethod(request) + FAILED, e);
            throw new ServletException(e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            Map<String, String> parameters = getRequestParams(request);
            parameters.put(REPLICATE, REPLICATE);
            replicate(context.getCurrentNodeIndex(), context.getCurrentNodeUrl(),
                    getMethod(request), context.getNodes(), context.getNodeStatuses(), parameters);
        } catch (Exception e) {
            log.error(this.getClass().getName() + getMethod(request) + FAILED, e);
            throw new ServletException(e);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    public HttpResponse replicate(int currentNodeIndex, String currentNodeUrl, String method, Properties nodes, Map<String, Boolean> nodeStatuses, Map<String, String> parameters) throws ClusterException {
        return ClusterHttpClient.replicate(currentNodeIndex, currentNodeUrl, method, nodes, nodeStatuses, parameters);
    }
}
