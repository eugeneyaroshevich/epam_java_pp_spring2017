package by.training.customerapi.dao;

public class DaoFactory {

    public static CustomerDao getCustomerDaoImpl() {
        return new CustomerDaoImpl();
    }
}
