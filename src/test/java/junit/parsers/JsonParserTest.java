package junit.parsers;

import by.training.customerapi.entity.Customer;
import com.google.gson.JsonObject;
import org.junit.Before;
import org.junit.Test;
import by.training.parser.JsonParser;

import static org.junit.Assert.*;

public class JsonParserTest {

    private Customer customer;
    private String jsonStringCustomer;

    @Before
    public void setUp() throws Exception {

        customer = new Customer(0, "Bean1", "Binich1", 40);
        jsonStringCustomer = "{\"id\":0,\"name\":\"Bean1\",\"surname\":\"Binich1\",\"age\":40}";
    }

    @Test
    public void testNewInstance() {

        JsonParser jsonParser = new JsonParser();

        assertNotNull(jsonParser);
    }

    @Test
    public void testToJson() throws Exception {

        String jsonCustomer = JsonParser.toJson(customer);

        assertNotNull(jsonCustomer);
        assertEquals(jsonStringCustomer, jsonCustomer);
    }

    @Test
    public void testFromJsonJsonObject() throws Exception {

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("id", 0);
        jsonObject.addProperty("name", "Bean1");
        jsonObject.addProperty("surname", "Binich1");
        jsonObject.addProperty("age", 40);

        Customer fromJsonCustomer = JsonParser.fromJson(jsonObject, Customer.class);

        assertNotNull(fromJsonCustomer);
        assertTrue(fromJsonCustomer instanceof Customer);
        assertEquals(customer, fromJsonCustomer);
        assertEquals(customer.getId(), fromJsonCustomer.getId());
        assertEquals(customer.getName(), fromJsonCustomer.getName());
        assertEquals(customer.getSurname(), fromJsonCustomer.getSurname());
        assertEquals(customer.getAge(), fromJsonCustomer.getAge());
    }


    @Test
    public void testFromJsonString() throws Exception {

        Customer fromJsonCustomer = JsonParser.fromJson(jsonStringCustomer, Customer.class);

        assertNotNull(fromJsonCustomer);
        assertTrue(fromJsonCustomer instanceof Customer);
        assertEquals(customer, fromJsonCustomer);
        assertEquals(customer.getId(), fromJsonCustomer.getId());
        assertEquals(customer.getName(), fromJsonCustomer.getName());
        assertEquals(customer.getSurname(), fromJsonCustomer.getSurname());
        assertEquals(customer.getAge(), fromJsonCustomer.getAge());
    }
}