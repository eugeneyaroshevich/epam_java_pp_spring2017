package junit.reflection.a;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import by.training.reflection.a.Printable;
import by.training.reflection.a.Printer;
import by.training.reflection.a.ProxyInvocationHandler;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import static org.junit.Assert.*;


public class ProxyInvocationHandlerTest {

    private Printable printer;
    private ProxyInvocationHandler handler;

    @Before
    public void setUp() throws Exception {
        printer = new Printer();
        handler = new ProxyInvocationHandler(printer);
    }

    @After
    public void tearDown() throws Exception {
        printer = null;
        handler = null;
    }

    // new Instance
    @Test
    public void testNewInstanceProxyInvocationHandlerNotNullReturned() throws Exception {
        ProxyInvocationHandler handler = new ProxyInvocationHandler(new Printer());

        assertNotNull(handler);
    }

    @Test
    public void testNewInstanceProxyInvocationHandlerFieldObjNotNullReturned() throws Exception {

        ProxyInvocationHandler handler = new ProxyInvocationHandler(new Printer());
        Field field = handler.getClass().getDeclaredField("obj");

        field.setAccessible(true);
        Object valueOfField = field.get(handler);
        field.setAccessible(false);

        assertNotNull(valueOfField);
    }

    @Test
    public void testNewInstanceProxyInvocationHandlerFieldObjNullReturned() throws Exception {

        ProxyInvocationHandler handler = new ProxyInvocationHandler(null);
        Field field = handler.getClass().getDeclaredField("obj");

        field.setAccessible(true);
        Object valueOfField = field.get(handler);
        field.setAccessible(false);

        assertNull(valueOfField);
    }


    // invoke()
    @Test
    public void testInvokeNotNullReturned() throws Throwable {

        Method method = printer.getClass().getMethod("print", String.class);

        Object[] args = new Object[]{"Test"};

        Object returnObject = handler.invoke(new Object(), method, args);

        assertNotNull(returnObject);
    }

    @Test
    public void testInvokeTestEqualReturnedValue() throws Throwable {

        Method method = printer.getClass().getMethod("print", String.class);

        Object[] args = new Object[]{"Test"};

        Object returnObject = handler.invoke(new Object(), method, args);

        assertEquals("Test", returnObject);
    }

    @Test(expected = NoSuchMethodException.class)
    public void testInvokeTestThrowNoSuchMethodException() throws Throwable {

        Method method = printer.getClass().getMethod("prin", String.class);

        Object[] args = new Object[]{"Test"};

        handler.invoke(new Object(), method, args);

    }

    @Test(expected = Throwable.class)
    public void testInvokeTestThrowThrowableException() throws Throwable {

        Method method = printer.getClass().getMethod("print", String.class);

        Object[] args = new Object[]{};

        handler.invoke(new Object(), method, args);

    }
}