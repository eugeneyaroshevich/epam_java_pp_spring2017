package by.training.jsptag;

import javax.servlet.ServletContext;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;


public class ResolveUrlTagHandler extends SimpleTagSupport {

    private String url;

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void doTag() throws JspException, IOException {

        PageContext pageContext = (PageContext) getJspContext();
        ServletContext servletContext = pageContext.getServletContext();

        pageContext.setAttribute("url", url);
        pageContext.setAttribute("resource", servletContext.getResource(url).getPath());

        getJspBody().invoke(null);
    }
}
