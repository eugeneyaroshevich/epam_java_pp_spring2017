package by.training.validator;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;

import javax.xml.stream.*;
import java.util.*;
import java.io.*;
import java.util.regex.Pattern;

public class BuildFileValidatorStax extends Task {

    private boolean checkdepends;
    private boolean checkdefault;
    private boolean checknames;

    public boolean isCheckdepends() {
        return checkdepends;
    }

    public void setCheckdepends(boolean checkdepends) {
        this.checkdepends = checkdepends;
    }

    public boolean isCheckdefault() {
        return checkdefault;
    }

    public void setCheckdefault(boolean checkdefault) {
        this.checkdefault = checkdefault;
    }

    public boolean isChecknames() {
        return checknames;
    }

    public void setChecknames(boolean checknames) {
        this.checknames = checknames;
    }


    private class CheckBean {

        private List<Property> properties;
        private Sequential sequential;

        public void setProperties(List<Property> properties) {
            this.properties = properties;
        }

        public void setSequential(Sequential sequential) {
            this.sequential = sequential;
        }

        public List<String> checkAll(boolean checkdepends, boolean checkdefault, boolean checknames) {

            List<String> checkList = new ArrayList<>();

            String checkString;

            if (!checkdepends) {
                checkString = "Check depends verification was not performed";
                checkList.add(checkString);
                System.out.println(checkString);
            } else {
                checkList.add(String.valueOf(checkdepends()));
            }
            if (!checkdefault) {
                checkString = "Check default verification was not performed";
                checkList.add(checkString);
                System.out.println(checkString);
            } else {
                checkList.add(String.valueOf(checkdefault()));
            }
            if (!checknames) {
                checkString = "Check names verification was not performed";
                checkList.add(checkString);
                System.out.println(checkString);
            } else {
                checkList.add(String.valueOf(checknames()));
            }

            return checkList;
        }

        private boolean checkdepends() {

            if ("javac".equals(sequential.getDepends().get(0)) &&
                    "jar".equals(sequential.getDepends().get(1)) &&
                    "java".equals(sequential.getDepends().get(2))) {
                System.out.println("Check depends is OK!");
                return true;
            }
            System.out.println("Check depends failed!");
            return false;

        }

        private boolean checkdefault() {

            boolean check = true;
            for (Property property : properties) {
                if (property.getName() == null || property.getLocation() == null) {
                    System.out.println("Check default failed!");
                    check = false;
                    break;
                }
            }
            if (check) {
                System.out.println("Check default is OK!");
                return true;
            }
            return false;

        }

        private boolean checknames() {

            boolean check = true;
            for (Property property : properties) {
                String value = property.getLocation();
                final String CHECKNAME_REGEX = "^[a-zA-Z_.-]*$";
                if (value == null || !Pattern.compile(CHECKNAME_REGEX).matcher(property.getLocation()).matches()) {
                    System.out.println("Check checknames failed!");
                    check = false;
                    break;
                }
            }
            if (check) {
                System.out.println("Check checknames is OK!");
                return true;
            }
            return false;
        }

        @Override
        public String toString() {
            return "CheckBean{" +
                    "properties=" + properties.toString() +
                    ", sequential=" + sequential.toString() +
                    '}';
        }
    }

    private class Property {

        private String name;
        private String location;

        public Property(String name, String location) {
            this.name = name;
            this.location = location;
        }

        public String getName() {
            return name;
        }


        public String getLocation() {
            return location;
        }


        @Override
        public String toString() {
            return "Property{" +
                    "name='" + name + '\'' +
                    ", location='" + location + '\'' +
                    '}';
        }
    }

    private class Sequential {

        private List<String> depends = new ArrayList<>(3);

        public List<String> getDepends() {
            return depends;
        }

        @Override
        public String toString() {
            return "Sequential{" +
                    "depends=" + depends.toString() +
                    '}';
        }
    }

    // method opened (public) for tests -> after close
    public CheckBean parseBuilds(String fileName) throws FileNotFoundException, XMLStreamException {

        CheckBean checkBean;

        if (fileName == null) {
            return null;
        }
        checkBean = new CheckBean();
        List<Property> properties = new ArrayList<>();
        Sequential sequential = new Sequential();

        XMLInputFactory factory = XMLInputFactory.newInstance();
        XMLStreamReader reader = factory.createXMLStreamReader(new FileReader(fileName));

        while (reader.hasNext()) {
            int eventType = reader.next();
            switch (eventType) {
                case XMLStreamReader.START_ELEMENT: {

                    String localName = reader.getLocalName();

                    switch (localName) {
                        case "property": {
                            Property property = new Property(reader.getAttributeValue(0), reader.getAttributeValue(1));
                            properties.add(property);
                            break;
                        }
                        case "sequential": {
                            while (reader.hasNext()) {
                                int eventType2 = reader.next();
                                if (eventType2 == XMLStreamReader.START_ELEMENT) {
                                    String localName2 = reader.getLocalName();
                                    switch (localName2) {
                                        case "javac": {
                                            sequential.getDepends().add(localName2);
                                            break;
                                        }
                                        case "jar": {
                                            sequential.getDepends().add(localName2);
                                            break;
                                        }
                                        case "java": {
                                            sequential.getDepends().add(localName2);
                                            break;
                                        }
                                    }
                                }
                            }
                            break;
                        }
                    }
                }
                case XMLStreamReader.END_DOCUMENT: {
                    checkBean.setProperties(properties);
                    checkBean.setSequential(sequential);
                }
            }
        }
        return checkBean;
    }

    @Override
    public void execute() throws BuildException {

        if (getBuildfiles().isEmpty()) {
            System.out.println("No BuildFiles");
            return;
        }

        try {
            for (Buildfile buildFile : getBuildfiles()) {

                parseBuilds(buildFile.getLocation()).checkAll(isCheckdepends(), isCheckdefault(), isChecknames());

            }
        } catch (XMLStreamException | FileNotFoundException e) {
            throw new BuildException(e);
        }
    }

    private Collection<Buildfile> buildfiles = new ArrayList<>();

    public Collection<Buildfile> getBuildfiles() {
        return buildfiles;
    }

    public Buildfile createBuildFile() {
        Buildfile buildfile = new Buildfile();
        buildfiles.add(buildfile);
        return buildfile;
    }

    public class Buildfile {

        String location;

        public Buildfile() {
        }


        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            if (location == null) {
                throw new NullPointerException();
            }
            this.location = location;
        }
    }

}
