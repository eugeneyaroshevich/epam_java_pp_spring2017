package by.training.cache;

import by.training.constants.Constants;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


public class ConcurrentLFUCache<K, V> implements Cache<K, V> {

    private int capacity = Constants.DEFAULT_CAPACITY;

    private final Map<K, CacheEntry> cache = new HashMap<>(capacity);


    public ConcurrentLFUCache() {
    }

    public ConcurrentLFUCache(final int capacity) {
        this.capacity = capacity;
    }


    public static <K, V> ConcurrentLFUCache<K, V> newInstance() {
        return new ConcurrentLFUCache<>();
    }

    public static <K, V> ConcurrentLFUCache<K, V> newInstance(int capacity) {
        return new ConcurrentLFUCache<>(capacity);
    }


    @Override
    public synchronized V put(K key, V value) {
        if (cache.size() == capacity) {
            K k = getDeletionKey();
            cache.remove(k);
        }
        CacheEntry cacheEntry = cache.put(key, new CacheEntry(key, value, System.nanoTime()));

        if (cacheEntry == null) {
            return null;
        }
        return cacheEntry.getValue();
    }

    @Override
    public synchronized V get(K key) {
        CacheEntry entry = cache.get(key);
        if (entry != null) {
            entry.frequency += Constants.ONE;
            entry.lastTime = System.nanoTime();
            return entry.getValue();
        }
        return null;
    }

    @Override
    public V remove(K key) {
        CacheEntry cacheEntry = cache.remove(key);
        if (cacheEntry == null) {
            return null;
        }
        return cacheEntry.getValue();

//        or if to use containsKey method

//        CacheEntry cacheEntry = null;
//        if (containsKey(key)) {
//            cacheEntry = cache.remove(key);
//        }
//        return cacheEntry != null ? cacheEntry.getValue() : null;
    }

    // not used
    @Override
    public boolean containsKey(K key) {
        return cache.containsKey(key);
    }

    @Override
    public int size() {
        return cache.size();
    }

    @Override
    public String toString() {
        return cache.toString();
    }


    private K getDeletionKey() {
        CacheEntry min = Collections.min(cache.values());
        return min.getKey();
    }


    private class CacheEntry implements Comparable<CacheEntry> {
        private final K key;
        private final V value;
        private Integer frequency;
        private Long lastTime;

        CacheEntry(K key, V value, Long lastTime) {
            this.key = key;
            this.value = value;
            this.frequency = Constants.ONE;
            this.lastTime = lastTime;
        }

        public K getKey() {
            return key;
        }

        public V getValue() {
            return value;
        }

        public int compareTo(CacheEntry o) {
            int lastCacheEntry = frequency.compareTo(o.frequency);
            return lastCacheEntry != Constants.ZERO ? lastCacheEntry : lastTime.compareTo(o.lastTime);
        }

        @Override
        public String toString() {
            return " is key, " + value + " is value";
        }
    }
}
