package by.training.reflection.a;


import java.lang.reflect.InvocationTargetException;

public class Runner {


    public static void main(String[] args) {

        try {

            Printable printer = new Printer();
            Printable printerProxy = new PrinterProxy();
            Printable proxyOfPrinter = (Printable) ProxyFactory.getInstanceOf(Printer.class);
            Printable proxyOfPrinterProxy = (Printable) ProxyFactory.getInstanceOf(PrinterProxy.class);

            printer.print("Printer");

            // out:

            // Printer

            printerProxy.print("Printer");

            // out:

            // Printer
            // PrinterProxy

            proxyOfPrinter.print("Printer");

            // out:

            // I am Proxy!
            // before method print
            // Printer
            // after method print

            proxyOfPrinterProxy.print("Printer");

            // out:

            // I am Proxy!
            // before method print
            // Printer
            // PrinterProxy
            // after method print

        } catch (ClassNotFoundException | NoSuchMethodException | InvocationTargetException | IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        }
    }

}
