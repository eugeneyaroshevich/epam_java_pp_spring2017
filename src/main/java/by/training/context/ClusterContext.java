package by.training.context;

import by.training.exception.ClusterException;
import by.training.network.ClusterHttpClientAlternate;
import by.training.service.JsonDataService;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpOptions;
import org.apache.http.client.methods.HttpPost;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static by.training.constants.Constants.*;
import static by.training.loader.ClusterLoader.*;
import static by.training.network.ClusterHttpClient.*;
import static by.training.util.ClusterUtil.*;


public final class ClusterContext implements AutoCloseable {

    private static final Logger log = Logger.getLogger(ClusterContext.class);

    @Autowired
    JsonDataService jsonDataService;

    private final String currentNodeUrl;
    private final Properties nodes;
    private Boolean clusterStatus;
    private Boolean synchroStatus;
    private final Map<String, Boolean> nodeStatuses = new ConcurrentHashMap<>();
    private final Map<String, Boolean> synchroStatuses = new ConcurrentHashMap<>();
    private final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();

    public ClusterContext(String contextPath) throws ClusterException {
        try {
            currentNodeUrl = getCurrentNodeUrl(contextPath);
            nodes = loadProperties(NODE_PROPERTIES);
            clusterStatus = loadClusterStatus();
            runStatusController(nodes, nodeStatuses, loadProperties(TIMER_PROPERTIES));
        } catch (ClusterException e) {
            log.warn(INIT_CLUSTER_CONTEXT_IS_FAILED);
            throw new ClusterException(INIT_CLUSTER_CONTEXT_IS_FAILED, e);
        }
    }


    public static ClusterContext build(String contextPath) throws ClusterException {
        return new ClusterContext(contextPath);
    }

    private String getEnvironmentVariable(String name) {
        String envVariable = System.getenv(name);
        if (envVariable == null) {
            log.warn(ENVIRONMENT_VARIABLE_NOT_FOUND);
            throw new NullPointerException(ENVIRONMENT_VARIABLE_NOT_FOUND);
        }
        return envVariable;
    }

    private String getCurrentNodeUrl(String contextPath) {
        return getEnvironmentVariable(contextPath.substring(ONE));
    }

    public String getCurrentNodeUrl() {
        return currentNodeUrl;
    }

    public int getCurrentNodeIndex() throws ClusterException {
        int index;
        String currentNodeIndex;
        try {
            currentNodeIndex = nodes.getProperty(getCurrentNodeUrl());
            if (currentNodeIndex.isEmpty()) {
                log.warn(CURRENT_NODE_INDEX_NOT_FOUND);
                throw new NullPointerException(CURRENT_NODE_INDEX_NOT_FOUND);
            }
            index = Integer.parseInt(currentNodeIndex);
        } catch (NumberFormatException e) {
            log.warn(GET_CURRENT_NODE_INDEX_FAILED);
            throw new ClusterException(GET_CURRENT_NODE_INDEX_FAILED, e);
        }
        return index;
    }

    public Properties getNodes() {
        return nodes;
    }

    public int getNodesSize() {
        return new HashSet(nodes.values()).size();
    }

    public Map<String, Boolean> getNodeStatuses() {
        if (nodeStatuses.isEmpty()) {
            log.warn(SET_NODE_STATUSES_NOT_FINISHED);
            throw new NullPointerException(SET_NODE_STATUSES_NOT_FINISHED);
        }
        return nodeStatuses;
    }

    public boolean isNodeStatuses() {
        return !nodeStatuses.containsValue(false);
    }

    private void setNodeStatuses(Properties nodes, Map<String, Boolean> nodeStatuses) throws ClusterException {
        for (String url : nodes.stringPropertyNames()) {
            if (getCurrentNodeUrl() == null || url.equals(getCurrentNodeUrl())) {
                continue;
            }
            boolean status = false;
            HttpResponse response = send(url, HttpOptions.METHOD_NAME, Collections.EMPTY_MAP);
            if (HttpStatus.SC_OK == response.getStatusLine().getStatusCode()) {
                status = true;
            }
            nodeStatuses.put(url, status);
        }
    }

    public Boolean isClusterStatus() {
        return clusterStatus;
    }

    private boolean loadClusterStatus() throws ClusterException {
        Boolean clusterStatus;
        try {
            clusterStatus = Boolean.valueOf(loadProperties(STATUS_PROPERTIES).getProperty(CLUSTER_STATUS));
            if (clusterStatus == null) {
                log.warn(DON_T_SET_CLUSTER_STATUS);
                throw new NullPointerException(DON_T_SET_CLUSTER_STATUS);
            }
        } catch (ClusterException e) {
            log.warn(LOAD_CLUSTER_STATUS_IS_FAILED);
            throw new ClusterException(LOAD_CLUSTER_STATUS_IS_FAILED, e);
        }
        return clusterStatus;
    }

    private void setClusterStatus(Boolean clusterStatus) {
        this.clusterStatus = clusterStatus;
    }


    private void setSynchroStatus(Boolean synchroStatus) {
        this.synchroStatus = synchroStatus;
    }

    public Boolean isSynchroStatus() {
        return synchroStatus;
    }

    private boolean isSynchroStatuses() {
        return !synchroStatuses.containsValue(false);
    }

    private void setSynchroStatuses(Properties nodes, Map<String, Boolean> synchroStatuses) throws ClusterException {
        for (String url : nodes.stringPropertyNames()) {
            if (url.equals(getCurrentNodeUrl())) {
                continue;
            }
            if (synchroStatuses.isEmpty() || synchroStatuses.get(url) == null || !synchroStatuses.get(url)) {
                boolean status = false;
                Map<String, String> parameters = new HashMap<>();
                parameters.put(STATUS, SYNCHRONIZE);
                HttpResponse response = send(url, HttpGet.METHOD_NAME, parameters);
                if (HttpStatus.SC_OK == response.getStatusLine().getStatusCode()) {
                    status = true;
                }
                synchroStatuses.put(url, status);
            }
        }
    }

    private void runStatusController(Properties nodes, Map<String, Boolean> nodeStatuses, Properties properties) {
        TimerTask periodicTask = new TimerTask() {
            public void run() {
                try {
                    setNodeStatuses(nodes, nodeStatuses);
                    if (isNodeStatuses() && isSynchroStatus() == null) {
                        synchronize();
                    }
                    setSynchroStatuses(nodes, synchroStatuses);
                    if (!isClusterStatus() && isNodeStatuses() && isSynchroStatuses()) {
                        setClusterStatus(Boolean.TRUE);
                        writeStatus(STATUS_PROPERTIES, CLUSTER_STATUS, isClusterStatus());
                    }
                } catch (ClusterException e) {
                    log.warn(RUN_STATUS_CONTROLLER_FAILED);
                    e.printStackTrace();
                }
            }
        };
        final long initialDelay = Long.parseLong(properties.getProperty(INITIAL_DELAY));
        final long period = Long.parseLong(properties.getProperty(PERIOD));
        executor.scheduleAtFixedRate(periodicTask, initialDelay, period, TimeUnit.SECONDS);
    }


    private void synchronize() throws ClusterException {
        Map<String, String> datas = jsonDataService.getAll();
        for (Map.Entry<String, String> data : datas.entrySet()) {
            Integer routeIndex = getRouteNodeIndex(data.getKey(), getNodesSize());
            Map<String, String> parameters = createRequestParams(data.getKey(), data.getValue(), SYNCHRO);
            if (getCurrentNodeIndex() != routeIndex) {

                HttpResponse routeResponse = route(routeIndex, HttpGet.METHOD_NAME, getNodes(), nodeStatuses, parameters);
                if (HttpStatus.SC_OK != routeResponse.getStatusLine().getStatusCode()
                        || !data.getValue().equals(getResponseData(routeResponse))) {

                    routeResponse = route(routeIndex, HttpPost.METHOD_NAME, getNodes(), nodeStatuses, parameters);
                }
                if (routeResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                    jsonDataService.delete(data.getKey());
                }
            } else {
                parameters.put(REPLICATE, REPLICATE);

                HttpResponse containsRouteResponse = replicate(routeIndex, getCurrentNodeUrl(), HttpGet.METHOD_NAME, getNodes(), nodeStatuses, parameters);
                if (HttpStatus.SC_OK != containsRouteResponse.getStatusLine().getStatusCode()
                        || !data.getValue().equals(getResponseData(containsRouteResponse))) {

                    replicate(routeIndex, getCurrentNodeUrl(), HttpPost.METHOD_NAME, getNodes(), nodeStatuses, parameters);
                }
            }
        }
        setSynchroStatus(true);
    }


    @Override
    public void close() throws Exception {
        executor.shutdown();
        log.info(SINGLE_THREAD_EXECUTOR_SERVICE_SHUTDOWN);
    }
}