package by.training.reflection.b;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public final class SerializationUtils {


    public static <T extends Comparable<T>> Map<String, Object> serialize(T object) throws IntrospectionException, InvocationTargetException, IllegalAccessException {

        if (object == null) {
            throw new NullPointerException("The object must not be null");
        }

        if (!object.getClass().isAnnotationPresent(Serialize.class)) {
            throw new IllegalArgumentException("The object can not be serialized. There is no @Serialize annotation");
        }

        Map<String, Object> result = new HashMap<>();

        Field[] declaredFields = object.getClass().getDeclaredFields();
        for (Field field : declaredFields) {
            if (!field.isAnnotationPresent(Serialize.class)) {
                continue;
            }
            String attributeName = field.getName();
            Method getterOfField = new PropertyDescriptor(attributeName, object.getClass()).getReadMethod();

            String annotationAlias = field.getAnnotation(Serialize.class).alias();

            if (annotationAlias.isEmpty()) {
                annotationAlias = attributeName;
            }

            result.put(annotationAlias, getterOfField.invoke(object));
        }
        return result;
    }

    public static <T extends Comparable<T>> T deserialize(Map<String, Object> serializeMap, Class<T> deserializeClass) throws IllegalAccessException, InstantiationException, IntrospectionException, InvocationTargetException {

        if (serializeMap == null) {
            throw new NullPointerException("The object must not be null");
        }

        if (serializeMap.isEmpty()) {
            throw new IllegalArgumentException("The object must not be empty");
        }

        T deserializeObject = deserializeClass.newInstance();

        for (Field field : deserializeClass.getDeclaredFields()) {
            if (field.isAnnotationPresent(Serialize.class)) {
                String annotationAlias = field.getAnnotation(Serialize.class).alias();

                if (annotationAlias.isEmpty()) {
                    annotationAlias = field.getName();
                }

                if (serializeMap.containsKey(annotationAlias)) {

                    String attributeName = field.getName();
                    Method setterOfField = new PropertyDescriptor(attributeName, deserializeClass).getWriteMethod();
                    setterOfField.invoke(deserializeObject, serializeMap.get(annotationAlias));
                } else {
                    throw new IllegalArgumentException("Missing field value");
                }
            }
        }

        return deserializeObject;
    }

    public static <T extends Comparable<T>> boolean equalObjects(T serializeObject, T deserializeObject) throws IllegalAccessException {

        if (serializeObject == null || deserializeObject == null) {
            throw new IllegalArgumentException("The object must not be null");
        }

        boolean equal = true;

        for (Field field : serializeObject.getClass().getDeclaredFields()) {
            if (!field.isAnnotationPresent(Equal.class)) {
                throw new IllegalArgumentException("The objects can not be equals. There is no @Equal annotation");
            }
            field.setAccessible(true);
            if (field.get(serializeObject) == null || field.get(deserializeObject) == null) {
                throw new IllegalArgumentException("Missing field value");
            }
            Comparable comp = (Comparable) field.get(serializeObject);
            if (comp.compareTo(field.get(deserializeObject)) != 0) {
                equal = false;
            }
        }
        return equal;
    }

}
