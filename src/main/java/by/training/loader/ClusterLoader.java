package by.training.loader;

import by.training.exception.ClusterException;
import org.apache.log4j.Logger;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.Properties;

public class ClusterLoader {

    private static final Logger log = Logger.getLogger(ClusterLoader.class);

    public static Properties loadProperties(String filename) throws ClusterException {
        Properties properties;
        InputStream in = null;
        try {
            properties = new Properties();
            in = ClusterLoader.class.getClassLoader().getResourceAsStream(filename);
            properties.load(in);
        } catch (IOException e) {
            log.error("load properties file is failed!", e);
            throw new ClusterException("load properties file is failed!",e);
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        log.info("properties file is loaded");
        return properties;
    }

    public static void writeStatus(String filename, String key, Boolean value) throws ClusterException {
        Properties properties;
        OutputStream output = null;
        try {
            properties = new Properties();
            URL url = ClusterLoader.class.getClassLoader().getResource(filename);
            if (url != null) {
                output = new FileOutputStream(url.getPath());
            }
            properties.setProperty(key, String.valueOf(value));
            if (output != null) {
                properties.store(output, null);
            }

        } catch (IOException e) {
            log.error("write properties file is failed!", e);
            throw new ClusterException("write properties file is failed!",e);
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }
}
