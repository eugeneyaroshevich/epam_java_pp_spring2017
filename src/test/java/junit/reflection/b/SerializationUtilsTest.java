package junit.reflection.b;

import org.junit.Before;
import org.junit.Test;
import by.training.reflection.b.Bean;
import by.training.reflection.b.SerializationUtils;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class SerializationUtilsTest {

    private Bean bean;
    private Bean serializeObject;

    @Before
    public void setUp() throws Exception {
        bean = new Bean();
        bean.setName("Bean");
        bean.setSurname("Binich");
        bean.setAge(50);

        serializeObject = new Bean("James", "Brown", 78, bean);
    }

    // new Instance
    @Test
    public void testNewInstance() {
        SerializationUtils utils = new SerializationUtils();

        assertNotNull(utils);
    }

    // serialize()
    @Test(expected = NullPointerException.class)
    public void testSerializeThrowNullPointerExceptionIfArgumentIsNull() throws Exception {

        // if argument of method is null
        SerializationUtils.serialize(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSerializeThrowIllegalArgumentExceptionIfNotAnnotationSerialize() throws Exception {

        // object which not contains @Serialize
        TestBeanNotSerializeAnnotation testBeanNotSerializeAnnotation = new TestBeanNotSerializeAnnotation();

        SerializationUtils.serialize(testBeanNotSerializeAnnotation);
    }

    @Test(expected = IntrospectionException.class)
    public void testSerializeThrowIntrospectionException() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, IntrospectionException {

        // object without any method
        TestBeanIntrospectionException bean = new TestBeanIntrospectionException("TestBeanIntrospectionException", "Binich", 35, new TestBeanIntrospectionException("InnerBean", "InnerBinich", 45, null));

        SerializationUtils.serialize(bean);
    }

    @Test
    public void testSerializeIfNotMarkedSerializeFieldOfBeanNotSerializeField() throws IllegalAccessException, IntrospectionException, InvocationTargetException {

        // object which not marked field @Serialize
        TestBeanIfNotMarkedSerializeFieldOfBean bean = new TestBeanIfNotMarkedSerializeFieldOfBean();
        bean.setName("Bean");
        bean.setSurname("Binich");
        bean.setAge(3);
        bean.setBean(new TestBeanIfNotMarkedSerializeFieldOfBean("InnerBean", "InnerBinich", 7, null));

        Map<String, Object> serialized = SerializationUtils.serialize(bean);

        assertTrue(serialized.keySet().contains("surname"));
        assertTrue(serialized.keySet().contains("age"));
        assertTrue(serialized.keySet().contains("bean"));

        assertFalse(serialized.keySet().contains("name"));
        assertNull(serialized.get("name"));
    }

    @Test
    public void testSerializeNotNullReturnedSerializedObjectAndContainsKeyEqualAliasOfSerializeAnnotationTrueReturned() throws IllegalAccessException, IntrospectionException, InvocationTargetException {

        Map<String, Object> serialisedMap = SerializationUtils.serialize(serializeObject);

        assertNotNull(serialisedMap);

        assertTrue(serialisedMap.keySet().contains("name"));
        assertTrue(serialisedMap.keySet().contains("surname"));
        assertTrue(serialisedMap.keySet().contains("age"));
        assertTrue(serialisedMap.keySet().contains("bean"));
    }

    @Test
    public void testSerializeIfAliasIsEmptyKeyEqualsFieldName() throws IllegalAccessException, IntrospectionException, InvocationTargetException {

        // object which contains field without alias (field "name")
        TestBeanIfAliasIsEmpty testBeanIfAliasIsEmpty = new TestBeanIfAliasIsEmpty();
        testBeanIfAliasIsEmpty.setName("name");
        testBeanIfAliasIsEmpty.setName("surname");
        testBeanIfAliasIsEmpty.setAge(35);
        testBeanIfAliasIsEmpty.setBean(null);

        Map<String, Object> serialisedMap = SerializationUtils.serialize(testBeanIfAliasIsEmpty);

        assertNotNull(serialisedMap);

        assertTrue(serialisedMap.keySet().contains("surname"));
        assertTrue(serialisedMap.keySet().contains("age"));
        assertTrue(serialisedMap.keySet().contains("bean"));

        assertTrue(serialisedMap.keySet().contains("name"));
    }

    @Test
    public void testSerializeNotNullReturnedSerializedObjectAndContainsValuesEqualOriginalTrueReturned() throws IllegalAccessException, IntrospectionException, InvocationTargetException {

        Map<String, Object> serialisedMap = SerializationUtils.serialize(serializeObject);

        assertNotNull(serialisedMap);

        assertTrue(serialisedMap.values().contains("James"));
        assertTrue(serialisedMap.values().contains("Brown"));
        assertTrue(serialisedMap.values().contains(78));
        assertTrue(serialisedMap.values().contains(bean));

        Bean innerBean = (Bean) serialisedMap.get("bean");

        assertSame(innerBean, bean);

        assertEquals(innerBean, bean);

        assertEquals(innerBean.getName(), bean.getName());
        assertEquals(innerBean.getSurname(), bean.getSurname());
        assertEquals(innerBean.getAge(), bean.getAge());
        assertNull(innerBean.getBean());
    }


    // test instance / consistency
    @Test
    public void testInstance() throws IllegalAccessException, IntrospectionException, InvocationTargetException, InstantiationException {

        Map<String, Object> serialized = SerializationUtils.serialize(serializeObject);
        Bean deserialized = SerializationUtils.deserialize(serialized, Bean.class);

        assertTrue(deserialized instanceof Bean);
        assertEquals(serializeObject, deserialized);

    }

    @Test
    public void testConsistency() throws IllegalAccessException, IntrospectionException, InvocationTargetException, InstantiationException {

        Map<String, Object> serialized1 = SerializationUtils.serialize(serializeObject);
        Map<String, Object> serialized2 = SerializationUtils.serialize(serializeObject);

        Bean deserialized1 = SerializationUtils.deserialize(serialized1, Bean.class);
        Bean deserialized2 = SerializationUtils.deserialize(serialized2, Bean.class);

        assertEquals(deserialized1, deserialized2);
        assertEquals(serializeObject, deserialized1);
        assertEquals(serializeObject, deserialized2);
    }

    // deserialize()
    @Test(expected = NullPointerException.class)
    public void testDeserializeThrowNullPointerExceptionIfNotAnnotationSerialize() throws Exception {

        SerializationUtils.deserialize(null, Bean.class);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDeserializeThrowIllegalArgumentExceptionIfIsEmptyArg() throws Exception {

        // object which not contains @Serialize
        SerializationUtils.deserialize(new HashMap<>(), Bean.class);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDeserializeThrowIllegalArgumentExceptionIfMapOfValuesIsNull() throws Exception {

        // object which not contains @Serialize
        Map<String, Object> map = new HashMap<>();
        map.put("name", null);

        SerializationUtils.deserialize(map,Bean.class);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDeserializeThrowIllegalArgumentExceptionIfMapOfValuesIsEmpty() throws Exception {

        // object which not contains @Serialize
        Map<String, Object> map = new HashMap<>();

        SerializationUtils.deserialize(map,Bean.class);
    }
}